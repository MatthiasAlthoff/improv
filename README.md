# IMPROV
*Authors: Matthias Althoff, Andrea Giusti, Stefan Liu, Aaron Pereira*

This repository contains additional material for the research article *Instantaneous Creation of Safe Robots from Modules through Self-Programming and Self-Verification*. A detailed documentation of the provided data is given in the *Supplementary Materials* section of that article.

The material is structured as follows:

* **Experiments**: Data from the experiments on implementation of self programming and on comparing static safety zones to self-verification.
* **Hardware**: STL files of 3D-printed modules, and technical specification of Robot and Controller used for experiments.
* **Software**: Code for self-programming and optimal module composition. MATLAB R2018a required.
* **Videos**: Supplementary videos demonstrating the findings of this article.
