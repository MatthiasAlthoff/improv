% main - Script for searching for good configurations sequentially.

% Required Directories: modular-robot-toolbox

% Author:       Esra Icer, Stefan Liu
% Written:      15-November-2018
% Last update:  
% Last revision:---

%------------- BEGIN CODE --------------
clear all
warning('off','Coder:MATLAB:singularMatrix')
addpath('ConfigurationGenerator');
addpath('Kinematics');
addpath('ConfigurationChecker');
addpath('PathPlanning');
addpath('CollisionCheck');
addpath(genpath('/Users/stefanliu/PhD/modular-robot-toolbox'));

%Definition of the task
task=task_definition2;
d = 6; %robot degree of freedom
checkForCollision = 1; % Determine whether to check for collision

%% Generate configuration of modules
disp('generate configurations')
PComb=CompliantCombinationsSchunk(d); 

%% Check all generated configurations
disp('check configurations and generate solutions')
Solution = [];
for i=1:size(PComb,1)
    ModRobI = ModID2ModRob(PComb(i,:),'modulesSchunkForOpt.mat');
    ModRobI = crrctStrctOpt(ModRobI); % Correct ModRob structure
   
    
    [success,path]=AllCheck(d, ModRobI, [],task,checkForCollision);
    if (success==1) % Determine cost fuction
        
        % Compute Acceleration
        Tsample = 0.002;
        q = path(:,1:6);
        dq = path(:,7:12);
        ddq = [(dq(2:end,:)-dq(1:end-1,:))/Tsample;zeros(1,d)];
        
        % Insert solution
        ModRob.path = q';
        [ModRob.cost.overall,ModRob.cost.time,ModRob.cost.energy] = cost_configuration(q,dq,ddq,ModRobI,Tsample,task.payload);
        ModRob.conf = PComb(i,:);
        Solution=[Solution, ModRob];
        disp(['checked ',num2str(i),' of ',num2str(length(PComb(:,1)))])
    end
end

%% Plot best Solution

if isempty(Solution)
    disp('No solution');
    return
else
    % Get best robot
    cost_all = [Solution.cost];
    [~,best]=min([cost_all.overall]);
    bestRob = ModID2ModRob(Solution(best).conf,'modulesSchunkForOpt.mat');
    q_sim = timeseries(Solution(best).path',0.002:0.002:size(Solution(best).path,2)*0.002);
    Solution(best).cost % print cost
    task.name = 'Task 2';
    task.robot_name = 'IMPROV';
    plotModRob(q_sim,bestRob,'vertices',task)
end

%% save
Rob_cost = Solution(best).cost;
Rob_path = Solution(best).path;
Rob = bestRob;
save('task2_optimal','Rob_cost','Rob_path','Rob')


