requires including the modular-robot-toolbox

1) Specify Task by modifying, e.g., TaskSpecification/task_definition1.m
2) Run main.m to compute an optimal IMPROV module configuration for specified task
3) Run otherRobots.m to compute the cost of Schunk LWA4p, Kuka LWR4+, St�ubli TX90 given the same task

