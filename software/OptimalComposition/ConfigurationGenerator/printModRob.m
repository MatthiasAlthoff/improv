% print_ModRob : prints the module configuration
%
% Syntax:  
%
% Inputs: 
%         ModRob: Robot structure
% Example: 
%
% Other m-files required: none
% Subfunctions: none
%
% See also: 

% Author:       Stefan Liu
% Written:      15-November-2018
% Last update:  ---
% Last revision:---

%------------- BEGIN CODE --------------
function res = printModRob(ModRob)

ModRob_text = 'Base';
count = 0; 
for i=1:length(ModRob)
    if ModRob(i).Mod.Cplx > 1 && count+1 < ModRob(i).Mod.Cplx
        count = count+1;
    else
        ModRob_text = strcat(ModRob_text,'-',ModRob(i).Mod.name);
        count = 0;
    end
end

res = ModRob_text;
