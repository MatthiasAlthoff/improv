%CompliantCombinations: Generates all module permutations and filters for
%the Combinations that have only compliant interfaces between the joints and links
function PComb = CompliantCombinationsSchunk(dof)
    
    % Sort modules into Categories:
    % - Powerball (Pb): PB1, PB3
    % - Link (L): L1,L2,L8,L9
    % - Link-Extension (EX): L4,L5,L6
    % - Endeffector (EE): L3
    
    % Create empty module:
    L10 = proto_module;
    L10.Mod.ID = 0;
    L10.Mod.typ = 2;
    L10.Mod.Cplx = 1;
    
    % Hard coded module IDs
    modules.PB = [1;3];
    modules.L = [4;5;14;15];
    modules.EX = [6;7;8;0];
    modules.EE = [12];
    
    % Generate possible combinations:
    % 4DOF: PB1 - EX-L-EX - PB - EX-EE
    % 6DOF: PB1 - EX-L-EX - PB1 - EX-L-EX - PB -EX-EE
    % 8DOF: PB1 - EX-L-EX - PB1 - EX-L-EX - PB1 - EX-L-EX - PB -EX-EE
    PComb = [];
    for i=1:dof/2 % dof must be either 4,6,8        
        if i < dof/2
            % Powerball
            PComb = expand_all_possib(PComb, modules.PB(1));
            % Extension
            PComb = expand_all_possib(PComb, modules.EX);
            % Link
            PComb = expand_all_possib(PComb, modules.L);
            % Extension
            PComb = expand_all_possib(PComb, modules.EX);
        else % Last iteration
            if dof == 8
                PComb = expand_all_possib(PComb, modules.PB(2));
                PComb = expand_all_possib(PComb, modules.EX);
                PComb = expand_all_possib(PComb, modules.EE);
            else
                PComb = expand_all_possib(PComb, modules.PB);
                PComb = expand_all_possib(PComb, modules.EX);
                PComb = expand_all_possib(PComb, modules.EE);
            end
        end
    end
end

function res = expand_all_possib(source, vector)
%source, vector must be uint8

l = size(vector,1);
m = size(vector,2);
a = size(source,1);
b = size(source,2);
if isempty(source)
    res = vector;
else
    res = zeros(a*l,b+m,'uint8');
    for i=1:a
        res(l*(i-1)+1:i*l,:) = [repmat(source(i,:),l,1) vector];
    end
end

end
