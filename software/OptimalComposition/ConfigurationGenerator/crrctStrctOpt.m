function ModRob = crrctStrctOpt(ModRobI)
% Correct the structure of ModRob such that it is suitable for Optimization
B0 = proto_module();
B0.Mod.ID = 90;
B0.Pro.length = 0;
B0.Pro.radius = 0;
B0.Pro.p1 = zeros(3,1);
B0.Pro.p2 = zeros(3,1);
B0.Dj = rmfield(B0.Dj,'Ke');
L0 = proto_module();
L0.Mod.ID = 91;
L0.Pro.length = 0;
L0.Pro.radius = 0;
L0.Pro.p1 = zeros(3,1);
L0.Pro.p2 = zeros(3,1);
L0.Dj = rmfield(L0.Dj,'Ke');

l = length(ModRobI);

ModRobI = [ModRobI,B0,L0];
vec = l+1;
if ModRobI(1).Mod.typ == 1
    vec = [vec,1];
else
    % consider adding link modules to base
    error('first module is not a joint module');
end
count = 0;
count_l = 0;
for i=2:l
    if ModRobI(i).Mod.typ == 1 && ModRobI(i-1).Mod.typ == 2
        if count
            linkModule = assembleLinkMods(ModRobI(i-count:i-1),30+count_l);
            if count == 3
                linkModule.Pro = ModRobI(i-2).Pro; % approximation
            elseif count == 1
                linkModule.Pro = ModRobI(i-1).Pro; % approximation
            else %count == 2
                l1 = ModRobI(i-1).Pro.length;
                l2 = ModRobI(i-2).Pro.length;
                if l1>l2 
                    linkModule.Pro = ModRobI(i-1).Pro; % approximation
                else
                    linkModule.Pro = ModRobI(i-2).Pro; % approximation
                end
            end
            count_l = count_l+1;
            ModRobI = [ModRobI,linkModule];
            vec = [vec,l+2+count_l];
            count = 0;
        end
        vec = [vec,i];
    elseif ModRobI(i).Mod.typ == 1 && ModRobI(i-1).Mod.typ == 1
        vec = [vec,l+2,i];
    elseif ModRobI(i).Mod.typ == 2 && ModRobI(i-1).Mod.typ ~= 2
        count = 1;
    elseif ModRobI(i).Mod.typ == 2 && ModRobI(i-1).Mod.typ == 2
        count = count+1;
    elseif ModRobI(i).Mod.typ == 3
        if count % Link module between EE and last joint
            linkModule = assembleLinkMods(ModRobI(i-count:i),30+count_l);
            linkModule.Pro = ModRobI(i).Pro; % approximation
            linkModule.Pro.radius = linkModule.Pro.radius; % increase radius of EE
            linkModule.Mod.typ = 3;
            count_l = count_l+1;
            ModRobI = [ModRobI,linkModule];
            vec = [vec,l+2+count_l];
            count = 0;
        else
            vec = [vec,i];
        end
    end
end

ModRob = ModRobI(vec);

end