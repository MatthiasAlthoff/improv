% getTrajectory: compute the Trapezoidal trajectory given a task
% specification and a robot
%
% Syntax:
%
% Inputs:
%         robot        : Robot, given as a module composition
%         task         : specified task

% Example:
%
% Other m-files required:
% Subfunctions: EulerRot
%
% MAT-files required:
% See also:
%

% Author:       Stefan Liu
% Written:      15-November-2018
% Last update:  ---
% Last revision:---

function [path] = getTrajectory(robot,task)

path = [];

%% Kinematics
[DHext,Kinematics.B] = ModRob2DHext(robot);
[DHtab, jt] = DHext2DH(DHext);
dof = length(DHtab(:,1));
Ri = EulerRot(task.pi(4:6));
Rd = EulerRot(task.pd(4:6));
Kinematics.DHtab=DHtab(1:dof,:);
Kinematics.jt=jt(1:dof,:); %joint type
Kinematics.jl=[-pi*ones(dof,1), pi*ones(dof,1)];
[Kinematics.qi,info_i]=invkin_s(Kinematics.DHtab(1:dof,:),Kinematics.jt,task.pi(1:3),Ri,'jlimit',Kinematics.jl,'iter',100,'pos_th',0.01,'mode','inverse');
[Kinematics.qd,info_d]=invkin_s(Kinematics.DHtab(1:dof,:),Kinematics.jt,task.pd(1:3),Rd,'init',Kinematics.qi,'jlimit',Kinematics.jl,'iter',100,'pos_th',0.01,'mode','inverse');
Kinematics.KinPar.B     = Kinematics.B(1:3,1:3);
Kinematics.KinPar.DHext = DHext;
Kinematics.KinPar.NJ    = dof;
Kinematics.DynPar = ModRob2Dynpar(robot,Kinematics.KinPar.DHext);

%% path generation
[PathCheck]=FastTrapezoidalTraj(Kinematics, task);
feasible = not(info_i) & not(info_d);
if feasible
path = PathCheck.PosPath;
end


end

function R = EulerRot(angles)
% Return Rotation matrix given Euler angles

sa = sin(angles(1)); %alpha
ca = cos(angles(1));
sb = sin(angles(2)); %beta
cb = cos(angles(2));
sc = sin(angles(3)); %gamma
cc = cos(angles(3));

R = zeros(3,3);
R(1,:) = [ca*cc-sa*cb*sc,sa*cc+ca*cb*sc,sb*sc];
R(2,:) = [-ca*sc-sa*cb*cc,-sa*sc+ca*cb*cc,sb*cc];
R(3,:) = [sa*sb,-ca*sb,cb];

end

