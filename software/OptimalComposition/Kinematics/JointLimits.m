% Joint Limits : gives the joint limits of the given composition as a
% vector
%
% Syntax:  
%
% Inputs: 
%         dof           : degrees-of-freedom
%         ModRob        : current module composition
% Example: 
%
% Other m-files required: 
%                         
% Subfunctions: 
%
% MAT-files required    : 
%
% See also: 

% Author:       Esra Icer
% Written:      27-January-2016
% Last update:  ---
% Last revision:---

%------------- BEGIN CODE --------------
function [LL UL]=JointLimits(ModRob,dof)
LL=[];
UL=[];
for i=1:size(ModRob,2)
    if (ModRob(i).Mod.typ==1) %means joint
        LL=[LL; (ModRob(i).Kj.Ljl)];
        UL=[UL; (ModRob(i).Kj.Ujl)];
    end
end
end
