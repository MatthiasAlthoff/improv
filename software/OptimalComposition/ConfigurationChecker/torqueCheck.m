% torque_check: It performs torque test
%
% Syntax:  
%
% Inputs: 
%         task          : specified task
%         q             : joint angles
%         KinPar        : Kinematic parameters
%         DynPar        : Dynamic parameters
%
% Example: 
%
% Other m-files required: NE_mod_6_mex,NE_mod
% Subfunctions: 
%
%
% See also: 

% Author:       Esra Icer, Stefan Liu
% Written:      28-January-2016
% Last update:  15-November-2018 (SL, consider gravity vector)
% Last revision:--- 

function res=torqueCheck(q, task, KinPar,DynPar)

F=[0; 0; -task.payload; 0; 0; 0];
gravity = 9.81;
zeroN = zeros(KinPar.NJ,1);

if length(q) ==6
    tau = NE_mod_6_mex(q,zeroN,zeroN,zeroN,zeroN,KinPar,DynPar,gravity,F);
else
    tau = NE_mod(q,zeroN,zeroN,zeroN,zeroN,KinPar,DynPar,gravity,F);
end


OK=[];
%if OK=0 it fails
for i=1:KinPar.NJ
    if (abs(tau(i))>DynPar(i).DJ.tau_lim)
        OK=[OK;0];
    else
        OK=[OK;1];
    end
end
res=[OK];
