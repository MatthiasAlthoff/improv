% StaCheck: It performs static torque test
%
% Syntax:  
%
% Inputs: 
%         ModRob        : information about current robot
%         ModRobI       : current module composition
%         q_i           : initial joint angles
%         q_d           : desired joint angles
%         F             : payload
%         jt            : Id vector of joint types (0 for rev., 1 for prism.)
%         DHtab         : DH table
% Example: 
%
% Other m-files required: torque_check
%                         Jacobian
% Subfunctions: 
%
%
% See also: 

% Author:       Esra Icer, Stefan Liu
% Written:      28-January-2016
% Last update:  15-November-2018
% Last revision:---    

function [s]=StaticCheck(Kinematics, task)

[TC_i]=StaticCheck_pnt(Kinematics.qi, Kinematics, task);
[TC_d]=StaticCheck_pnt(Kinematics.qd, Kinematics, task);

    
if TC_i && TC_d
    s.success=1;
    %info='statically reachable';
else
    s.success=0;
    %info='static fault';
end
end
