function [success]=StaticCheck_pnt(q, Kinematics, task)

OK_i=torqueCheck(q, task, Kinematics.KinPar, Kinematics.DynPar);

if ((OK_i==ones(Kinematics.KinPar.NJ,1)))
    success=1;
else
    success=0;
end
end
