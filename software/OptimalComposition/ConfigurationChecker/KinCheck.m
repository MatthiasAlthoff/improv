% KinCheck: It checks if the position is kinematically reachable or not
%
% Syntax:  
%
% Inputs: 
%         ModRob        : current module composition
%         pose_i        : initial pose
%         pose_d        : desired pose
%         workspace     : 0-entire workspace
%                         1-reduced to x-y plane
%                         2-x y z 
%         angles        : 0-rad
%                         1-deg
%         type          : desired pose->RPY
% Example: 
%
% Other m-files required: ModRob2DH
%                         inv_Kin
%                         print_ModRob
% Subfunctions: 
%
% MAT-files required    : 
%
% Explanations: 1) ModRob2DH(ModRobI(2:end)) from (2:end) because in ModRob2DH base module
%               didn't considered in Andrea's code B matrix is for Base module
%               2) [a_i, alpha_i, gamma_i, d_i] (dofx4 matrices)
%               3) %OK shows that whether the given position is kinematically reachable. 
%               if info_i=0 , the given position reachable otherwise it is not reachable
%               For possible combinations, both the initial and desired positions should
%               be reachable for the robot.
% See also: 

% Author:       Esra Icer
% Written:      27-January-2016
% Last update:  12-October-2017
% Last revision:

%------------- BEGIN CODE --------------
function [k, ModRobI]=KinCheck(ModRobI,modules, task,dof)

    % Denavit Hartenberg Table(DHtab) is calculated 
    if isempty(modules)
        [DHext,k.B] = ModRob2DHext(ModRobI(1:end));
    else
        [DHext,k.B] = ModRob2DHext_fast(ModRobI(2:end),modules);
    end
    [DHtab, jt] = DHext2DH(DHext);
    k.DHtab=DHtab(1:dof,:);
    k.jt=jt(1:dof,:); %joint type

    % Inverse Kinematics Calculation
    % Joint limits for optimization problem
    [lb, ub]=JointLimits(ModRobI,dof);
    k.jl=[lb, ub];
    
    Ri = EulerRot(task.pi(4:6));
    Rd = EulerRot(task.pd(4:6));
    
    %Initial point in joint space
    [k.qi,info_i]=invkin_faster_dv_6_mex(k.DHtab(1:dof,:),task.pi(1:3),Ri,k.B,zeros(6,1),k.jl);
    %goal point in joint space
    [k.qd,info_d]=invkin_faster_dv_6_mex(k.DHtab(1:dof,:),task.pd(1:3),Rd,k.B,k.qi,k.jl);

    if (info_i==0) &&(info_d==0)                       
        k.success=1;    
    else
        k.success=0;
    end
    
    k.KinPar.B     = k.B(1:3,1:3);
    k.KinPar.DHext = DHext;
    k.KinPar.NJ    = dof;
    
end
    
    
function R = EulerRot(angles)
% Return Rotation matrix given Euler angles

sa = sin(angles(1)); %alpha
ca = cos(angles(1));
sb = sin(angles(2)); %beta
cb = cos(angles(2));
sc = sin(angles(3)); %gamma
cc = cos(angles(3));

R = zeros(3,3);
R(1,:) = [ca*cc-sa*cb*sc,sa*cc+ca*cb*sc,sb*sc];
R(2,:) = [-ca*sc-sa*cb*cc,-sa*sc+ca*cb*cc,sb*cc];
R(3,:) = [sa*sb,-ca*sb,cb];

end
    