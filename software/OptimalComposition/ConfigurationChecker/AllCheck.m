% AllCheck: It performs each tests step by step
%
% Syntax:
%
% Inputs:
%         ModRob        : current module composition
%         dof           : degree of freedom
%         modules       : available modules
%         task          : specified task
%         checkForCollision: flag for whether collision check should be
%                            performed
% Output:
%               success  : 1 passed the tests
%                          0 failed
% Example:
%
% Other m-files required: modular-robot-toolbox, KinCheck, StaticCheck,
%                         FastTrapezoidalTraj,CollisionCheck
% Subfunctions:
%
% MAT-files required:
%
%

% Author:       Esra Icer, Stefan Liu
% Written:      28-January-2016
% Last update:  15-November-2018 (SL)

function [success,path]=AllCheck(dof,ModRobI,modules,task,checkForCollision)
path = [];
success = 0;
%
%Kinematic test is applied
[Kinematics, ModRobI]=KinCheck(ModRobI,modules,task,dof);
%
if (Kinematics.success==1)
    %
    % Obtain dynamical model
    Kinematics.DynPar = ModRob2Dynpar(ModRobI,Kinematics.KinPar.DHext);
    
    % Static force/torque test is applied
    [Statics]=StaticCheck(Kinematics, task);
    %
    if (Statics.success==1)
        %
        %Obstacle-free path generation
        [PathCheck]=FastTrapezoidalTraj(Kinematics, task);
        %
        if (PathCheck.success==1)
            if checkForCollision == 1
                %Path generation in the environment with obstacles
                [ObstacleFreePath]= CollisionCheck(ModRobI, Kinematics, task, PathCheck.PosPath,PathCheck.PosPathVelocity);
                success=ObstacleFreePath.success;
                path = ObstacleFreePath.ObsAvPath;
            else
                success=1;
                path = [PathCheck.PosPath,PathCheck.PosPathVelocity];
            end
        end
    end
end

end
