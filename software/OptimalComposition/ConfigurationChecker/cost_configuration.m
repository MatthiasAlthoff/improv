function [G,t_fk,energy] = cost_configuration(q,dq,ddq,ModRob_k,Tsample,payload)
% Computes the Cost of ModRob k based on trajectory duration and energy expenditure
%
% Input:
%  - q,dq,ddq: Trajectory
%
% Output:
%  - G: Cost
%  - t_fk: Time (s) to perform this task
%  - energy: Energy (J) needed 

% Get Robot
[DHext,B]   = ModRob2DHext(ModRob_k);
DynPar = ModRob2Dynpar(ModRob_k,DHext);
KinPar.B     = B(1:3,1:3);
KinPar.DHext = DHext;
KinPar.NJ    = get_NJ(ModRob_k);
gravity      = -9.81; 
Fext = [0;0;-payload;0;0;0];

% Weights:
w1 = 1;
w2 = 0.2;

% Duration
t_fk = Tsample*length(q(:,1));

% Energy
energy = 0;
for i=1:20:length(q(:,1))
    if KinPar.NJ == 6
        u = NE_mod_6_mex(q(i,:)',dq(i,:)',dq(i,:)',zeros(KinPar.NJ,1),ddq(i,:)',KinPar,DynPar,gravity,Fext);
    else
        u = NE_mod(q(i,:)',dq(i,:)',dq(i,:)',zeros(KinPar.NJ,1),ddq(i,:)',KinPar,DynPar,gravity,Fext);
    end
%     energy = energy + sum(max(zeros(KinPar.NJ,1),u.*dq(i,:)'*Tsample*20)); % compute Energy in J
    energy = energy + sum(abs(u.*dq(i,:)'*Tsample*20)); % compute Energy in J
%     energy = energy + u'*u;
end

% Cost
G =  w1*t_fk + w2*energy;
end

