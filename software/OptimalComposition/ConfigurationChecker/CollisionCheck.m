function ObstacleAvoidance=CollisionCheck(ModRobI, Kinematics, task, q_d, dq_d)

    %% Collision model with capsules
    encl = zeros(7,size(q_d,2));
    for i=1:size(q_d,2)
        encl(1:3,i) = ModRobI(1+2*i).Pro.p1;
        encl(4:6,i) = ModRobI(1+2*i).Pro.p2;
        encl(7,i) = ModRobI(1+2*i).Pro.radius;
    end
    Kinematics.Enclosures = generateCollisionModel(ModRobI,'capsules');
    Kinematics.V = generateCollisionModel(ModRobI,'vertices');
    
    %% Compute Forward kinematics
    sampling = round(size(q_d,1)/50); % check only 50 points
    GenPath = q_d(1:sampling:size(q_d,1),:)';
    EE_pos = zeros(50,3);
    for i=1:size(GenPath,2)
        EE_pos(i,:)=fwdkin_b(GenPath(:,i), Kinematics.DHtab, Kinematics.jt, Kinematics.B)';
    end
  
    %% Collision check for End Effector:
    [coll_EE,coll_EE_i]=EE_CollisionCheck(EE_pos,task.obstacle,ModRobI);             %TaskSpace

    %% Collision check for Joints/Links/Self Collision:
    collM= JointLinkCollision(ModRobI, Kinematics, GenPath, task.obstacle);
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %There are four possible states:
    %State 1.  EE & Joint & Link collide the obstacle       (coll==1) && (collJ~=0)
    %State 2.  EE doesn't collide, Joint & Link collide   (coll==0) && (collJ~=0)
    %State 3.  EE collides, Joint & Link don't collide   (coll==1) && (collJ==0)
    %State 4.  EE & Joint & Link don't collide                    (coll==0) && (collJ==0)
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %States:
        %State 1:
        if ((coll_EE~=0) && (collM~=0))
            %disp('End effector and Joint/Link collision......Cancel the Configuration');
            success=0;
            ObsPath=[];
        %State 2:
        elseif ((coll_EE==0) && (collM~=0))
            %disp('Joint/Link collision......Cancel the Configuration');
            success=0;
            ObsPath=[];
            
        %State 3:
        elseif ((coll_EE~=0 && collM==0))
            % disp('Only end effector collision..... Generating a new path.......')
            % Regenerate path
            success=0;
            ObsPath = [];
            if ~isempty(ObsPath)
                success = 1;
            end
            
        %State 4:
        elseif ((coll_EE==0 && collM==0))
            success=1;
            ObsPath=[q_d,dq_d];
        end
        ObstacleAvoidance.success=success;
        ObstacleAvoidance.ObsAvPath=ObsPath;
%regenerated path
end
