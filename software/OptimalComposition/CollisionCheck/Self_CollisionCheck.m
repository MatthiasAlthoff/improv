% SelfCollision - Checks whether the links or the joints collide with each
% others
%
% Syntax:
%
% Inputs:
%         joint_positions: joint positions
%         ModRob        : current module composition
% Example:
%
% Other m-files required: DistBetween2Segment_mex
%
% Subfunctions: none
% Required Directories:
%
% MAT-files required:
%
% See also:

% Author:       Esra Icer, Stefan Liu
% Written:      29-January-2016
% Last update:  15-November-2018
% Last revision:---

%------------- BEGIN CODE --------------
function res=Self_CollisionCheck(link_pos_i,link_pos_e, radius)
SelfColl=0;
for i=1:size(link_pos_e,1)-1
    Pm_i=link_pos_i(i,:);
    Pm_i1=link_pos_e(i,:);
    for j=(i+2):size(link_pos_e,1)
        Pn_i=link_pos_i(j,:);
        Pn_i1=link_pos_e(j,:);
        d=DistBetween2Segment_mex(Pm_i,Pm_i1, Pn_i, Pn_i1);
        dLm=radius(i);
        dLn=radius(j);
        if (d>dLm+dLn)
            SelfColl=0;
            %disp('No Self Collision');
        else
            SelfColl=1;
            %disp(sprintf('Link %s and Link %d Collides with each other', (i), (j)));
            break
        end
    end
    if (SelfColl==1)
        break
    else
    end
end
res=SelfColl;
end
