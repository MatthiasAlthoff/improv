% EE_CollisionCheck - Checks the collision between the end effector and obstacles
%
% Syntax:  
%
% Inputs: 
%         EE_pos        : end effector's position in Cartesian coordinates
%         obstacle      : obstacle definition
% Example: 
%
% Other m-files required: none
% Subfunctions: none
% Required Directories:
%
% MAT-files required: 
%
% See also: 

% coll=1: there is a collision// coll=0 no collision

% Author:       Esra Icer
% Written:      29-January-2016
% Last update:  ---
% Last revision:---

%------------- BEGIN CODE --------------
function [coll,i,j] = EE_CollisionCheck(EE_pos, obstacle, ModRobI)
    coll=0;
    for i=1:length(EE_pos(:,1))
        if isempty(coll)
        break
        else
            for j=1:size(obstacle,1)
                d = sqrt((EE_pos(i,1)-obstacle(j,1))^2+(EE_pos(i,2)-obstacle(j,2))^2+(EE_pos(i,3)-obstacle(j,3))^2);  
                if (d<(obstacle(j,4)+ModRobI(end).Pro.radius))
                    coll=1;
                    return
                else
                    coll=0;
                end
            end
        end
    end
end


      