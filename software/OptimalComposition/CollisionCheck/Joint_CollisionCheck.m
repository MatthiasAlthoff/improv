% EE_CollisionCheck - Checks the collision between the end effector and obstacles
%
% Syntax:  
%
% Inputs: 
%         EE_pos        : end effector's position in Cartesian coordinates
%         obstacle      : obstacle definition
% Example: 
%
% Other m-files required: none
% Subfunctions: none
% Required Directories:
%
% MAT-files required: basic_Modules.mat
%                     obstacle.mat
%
% See also: 

% Author:       Esra Icer
% Written:      29-January-2016
% Last update:  ---
% Last revision:---

%------------- BEGIN CODE --------------
function [coll] = Joint_CollisionCheck(Joint_Pos, obstacle, ModRobI)

    coll=0;
    for i=1:size(Joint_Pos(:,1))
        if isempty(coll)
        break
        else
            for j=1:size(obstacle,1)
                d = sqrt((Joint_Pos(i,1)-obstacle(j,1))^2+(Joint_Pos(i,2)-obstacle(j,2))^2+(Joint_Pos(i,3)-obstacle(j,3))^2);  
                if (d<(obstacle(j,4)+ModRobI(i).Pro.radius))
                    coll=1;
                break
                else
                    coll=0;
                end
            end
        end
    end
end

