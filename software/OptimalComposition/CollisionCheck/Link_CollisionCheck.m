% point_to_line - Checks two lines collides with obstacles
% the obstacles
%
% Syntax:  
%
% Inputs: 
%         v1            : line 1
%         v2            : line 2
%         obstacle      : obstacle definition


% Example: 
%
% Other m-files required: segment_point_dist_3d
% Subfunctions: none
% Required Directories:
%
% MAT-files required: 
%
% See also: 

% Author:       Esra Icer
% Written:      29-January-2016
% Last update:  ---
% Last revision:---

%------------- BEGIN CODE --------------
function  [coll] = Link_CollisionCheck(v1,v2,obstacle,radius,pnt)
    coll=0;
    for i=1:size(obstacle,1)
        d=segment_point_dist_3d ( v1', v2', obstacle(i,1:3)');
        if d<(obstacle(i,4)+radius)
            coll=1;
            break
        else
            coll=0;
            %disp('No Link Collision');  
        end
    end
end

