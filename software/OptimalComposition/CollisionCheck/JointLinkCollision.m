% JointLinkCollision - Checks whether the links or the joints collid with
% the obstacles
%
% Syntax:  
%
% Inputs: 
%         ModRobI       : current module composition
%         kin           : struct containing Robot data
%         obstacle      : obstacle definition
%         path          : points obtained from the path planning algorithm
% Outputs:
%    info                      -> 0: Success
%                                 1: no convergence (no solution or 
%                                    not global minimum)
%                                 2: convergence, but joint limits violated
% Example: 
%
% Other m-files required: Joint_CollisionCheck
%                         Link_CollisionCheck
%                         Self_CollisionCheck
%                         
% Subfunctions: DH_trans_fast, Tr_z_fast
% Required Directories: modular-robot-toolbox
%
%
% See also: 

% Author:       Esra Icer
% Written:      03- Feb-2016
% Last update:  15- Nov-2018 (Stefan Liu, speed improvements, new collision model)
% Last revision:---

%------------- BEGIN CODE ---------------
function [res, info]=JointLinkCollision(ModRobI, kin,path, obstacle)  
    %obtain n_i val
    collL=0;
    collJ=0;

    %Translation Matrices
    for j=1:size(path,2)

       %Step 2: Check Link Collision    
        if ~isempty(obstacle)
            if (~collJ)

                robotV = fwdkinCollisionModel(path(:,j),kin.KinPar.DHext,kin.V,'vertices');
                vertices = [robotV{:}];
                dist = vecnorm(vertices-obstacle(1:3)',2,1);
                if any(dist<obstacle(4))    %If there is a collision between link and obstacle, it gives an error and breaks the code
                    res = 1;
                    return
                end
            end
        end
        %Self-collision check
        if ((~collL) && (~collJ)) 
            % new collision model
            robotCapsules = fwdkinCollisionModel(path(:,j),kin.KinPar.DHext,kin.Enclosures,'capsules');
            link_pos2_i = robotCapsules(1:3,2:end)';
            link_pos2_e = robotCapsules(4:6,2:end)';
            link_pos2_r = 0.5*kin.Enclosures(7,2:end)';
            link_pos2_r(end) = 2*link_pos2_r(end); % larger endeffector for self-coll
            
            SelfColl=Self_CollisionCheck(link_pos2_i,link_pos2_e,link_pos2_r);
            if (SelfColl)
                res = 1;
                return
            else
                SelfColl=0;
            end
        end
        
    end
    
    if ((~collL) && (~collJ) && (~SelfColl))
        collM=0;
    else
        collM=1;
    end
        res=collM;
end

function T = Tr_z_fast(L) %Translation at z

T = [1, 0, 0, 0;
     0, 1, 0, 0;
     0, 0, 1, L;
	 0, 0, 0, 1];

end

%DH tranformation
function D = DH_trans_fast(a, d,cos_alpha,sin_alpha,cos_theta,sin_theta)

D=[cos_theta, -sin_theta*cos_alpha, sin_theta*sin_alpha, a*cos_theta;...
    sin_theta, cos_theta*cos_alpha, -cos_theta*sin_alpha, a*sin_theta;...
    0, sin_alpha, cos_alpha, d;...
    0,0,0,1];

end