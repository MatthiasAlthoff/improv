% task_definition - Script for defining the task
%
% Syntax:  
%
% Inputs: 
%
% Example: 
%
% Other m-files required: none
% Subfunctions: none
% Required Directories:
%
% See also: 

% Author:       Esra Icer, Stefan Liu
% Written:      12-October-2017
% Last update:  15-November-2018 (SL)
% Last revision:---

%------------- BEGIN CODE --------------
function task=task_definition2
%Definition of the workspace
workspace = 2;             % 1-reduced to x-y plane; 0-entire workspace 2: x y z 
angles = 0;                % angles to be returned: 1-deg or 0-rad
type = 0;                  % desired pose->RPY

%Definition of the desired initial pose of end effector
%Desired position (x,y,z)
xi = 0.37;                    % end effector's position on x direction [m]
yi = -0.10;                    % end effector's position on y direction [m]
zi = 0.57;                  % end effector's position on z direction [m]

%Desired initial orientation (RPY) rad, interval for theta (-pi/2,pi/2)
phi_i = 0;                  % end effector's orientation on roll
theta_i = 0;              % end effector's orientation on pitch
psi_i = 0;                  % end effector's orientation on yaw

%Definition of the desired pose of end effector
%Desired position (x,y,z)
xd = 0.10;                    % end effector's position on x direction [m]
yd = 0.37;                    % end effector's position on y direction [m]
zd = 0.37;                  % end effector's position on z direction [m]


%Desired orientation (RPY) rad, interval for theta (-pi/2,pi/2)
phi_d = 0;                 % end effector's orientation on roll     [rad]
theta_d = pi/2;               % end effector's orientation on pitch    [rad]
psi_d = 0;                 % end effector's orientation on yaw      [rad]

%Desired position
pose_i = [xi; yi; zi; phi_i; theta_i; psi_i];
pose_d = [xd; yd; zd; phi_d; theta_d; psi_d];


%Desired Payload (Payload is accepted as in z direction)
Payload= 50;              % unit is N

obstacle1.origin_x=0.2;
obstacle1.origin_y=0.2;
obstacle1.origin_z=0.75;
obstacle1.radius_obs=0.1;


%Definition of Obstacles
safety_factor=0.05;
obstacle=[obstacle1.origin_x obstacle1.origin_y obstacle1.origin_z obstacle1.radius_obs+safety_factor];
task.workspace=workspace;
task.angles=angles;
task.type=type;

task.pi=pose_i;
task.pd=pose_d;

task.payload=Payload;
task.obstacle=obstacle;

task.time=1;
task.node=50;