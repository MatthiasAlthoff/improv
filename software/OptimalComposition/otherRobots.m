% otherRobots - Script for computing the cost of KUKA LWR 4+, Schunk LWA4p and Staubli TX90 given the same task

% Required Directories: modular-robot-toolbox

% Author:       Stefan Liu
% Written:      15-November-2018
% Last update:  
% Last revision:---

%------------- BEGIN CODE --------------


addpath(genpath('/Users/stefanliu/PhD/modular-robot-toolbox'));
addpath('Comparison')
addpath('TaskSpecification')
addpath('PathPlanning')

%% Specify task
task=task_definition1;
task.name = 'Task 1';

%% Load Robots
% Gripper
load('Gripper.mat');

n = 4; %sampling of rotating base

% KUKA LWR
KukaModules = load('modulesKukaLWR4p.mat');
Kuka_0 = [KukaModules.LWR4p_1;KukaModules.LWR4p_2;KukaModules.LWR4p_3;...
    KukaModules.LWR4p_4;KukaModules.LWR4p_5;KukaModules.LWR4p_6;KukaModules.LWR4p_7;SchunkPg70];
Kuka = sampleRotatingBase(Kuka_0,n);

% Schunk
SchunkModules = load('modulesSchunkForOpt.mat');
Schunk_0 = [SchunkModules.PB1;SchunkModules.L1;SchunkModules.PB2;SchunkModules.L2;SchunkModules.PB3;SchunkModules.L3];
Schunk = sampleRotatingBase(Schunk_0,n);

% Staubli
StaubliModules = load('modulesStaubliTX90.mat');
Staubli_0 = [StaubliModules.TX90_1;StaubliModules.TX90_2;StaubliModules.TX90_3;StaubliModules.TX90_4;...
    StaubliModules.TX90_5;StaubliModules.TX90_6;SchunkPg70];
Staubli = sampleRotatingBase(Staubli_0,n);

% Generate Path
for i=1:n
    SchunkPath{i} = getTrajectory(Schunk{i},task);
    StaubliPath{i} = getTrajectory(Staubli{i},task);
    KukaPath{i} = getTrajectory(Kuka{i},task);
end

%% Get Cost
for i=1:n
    Schunk_cost(i) = getCost(SchunkPath{i},Schunk{i},task.payload);
    Staubli_cost(i) = getCost(StaubliPath{i},Staubli{i},task.payload);
    Kuka_cost(i) = getCost(KukaPath{i},Kuka{i},task.payload);
end
[~,best_Schunk] = min([Schunk_cost.overall]);
[~,best_Kuka] = min([Kuka_cost.overall]);
[~,best_Staubli] = min([Staubli_cost.overall]);

%% Plot Schunk
task.robot_name = 'Schunk LWA 4p';
if SchunkPath{best_Schunk}
    q_Schunk_sim = timeseries(SchunkPath{best_Schunk},0.002:0.002:size(SchunkPath{best_Schunk},1)*0.002);
    plotModRob(q_Schunk_sim,Schunk{best_Schunk},'vertices',task)
    Schunk_cost(best_Schunk)
else
    disp('Schunk: no solution')
end

%% Plot Staubli
task.robot_name = 'St�ubli TX90';
if StaubliPath{best_Staubli}
    q_Staubli_sim = timeseries(StaubliPath{best_Staubli},0.002:0.002:size(StaubliPath{best_Staubli},1)*0.002);
    plotModRob(q_Staubli_sim,Staubli{best_Staubli},'vertices',task)
    Staubli_cost(best_Staubli)
else
    disp('Staubli: no solution')
end

%% Plot Kuka
task.robot_name = 'Kuka LWR 4+';
if KukaPath{best_Kuka}
    q_Kuka_sim = timeseries(KukaPath{best_Kuka},0.002:0.002:size(KukaPath{best_Kuka},1)*0.002);
    plotModRob(q_Kuka_sim,Kuka{best_Kuka},'vertices',task)
    Kuka_cost(best_Kuka)
else
    disp('Kuka: no solution')
end

%% Save results

% save('task1_otherRobots',...
%     'KukaPath','Kuka_cost','Kuka','best_Kuka',...
%     'SchunkPath','Schunk_cost','Schunk','best_Schunk',...
%     'StaubliPath','Staubli_cost','Staubli','best_Staubli');

function cost = getCost(path,robot,payload)
if path
NJ = get_NJ(robot);
dq_max = 1*ones(NJ,1);
ddq_max = 1*ones(NJ,1);
[q,dq] = trajKunz([path(1,:);path(end,:)],dq_max,ddq_max);
Tsample = 0.002;
ddq = [(dq(2:end,:)-dq(1:end-1,:))/Tsample;zeros(1,NJ)];
[cost.overall,cost.time,cost.energy] = cost_configuration(q,dq,ddq,robot,Tsample,payload);
else
    cost.overall = 100000000;
    cost.time = 10000000;
    cost.energy = 10000000;
end

end

function robot = sampleRotatingBase(robot_0,n)
% sample from -pi to pi -> create n robots with n different orientations of
% the base
rotations = linspace(-pi,pi,n+1);

robot = cell(1,n);
for i=1:n
    robot{i} = robot_0;
    robot{i}(1).Kj.delta_j = rotations(i);
end

end
