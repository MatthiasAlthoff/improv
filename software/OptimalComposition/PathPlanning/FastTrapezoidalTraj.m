% FastTrapezoidalTraj: Compute the fastest trapezoidal trajectory for
% specified task
%
% Syntax:
%
% Inputs:
%         kin           : struct containing robot info
%         task          : specified task
% Output:
%               traj    : trajectory (position and velocity)
%
% Example:
% Other m-files required: StaticCheck_pnt, trajKunz
% Subfunctions:
% MAT-files required:
%
%
% Author:       Esra Icer, Stefan Liu
% Written:      29-January-2016
% Last update:  15-November-2018 (SL)
% Last revision:---

%------------- BEGIN CODE --------------
function  [traj] = FastTrapezoidalTraj(kin, task)

    traj.success=0;
    % define initial and goal positions as a vector
    q_path =[kin.qi';kin.qd']; 

    %Step 1: Generate a path from qi to qf in joint space
    % Tsample = 0.002;
    dq_max = 1*ones(kin.KinPar.NJ,1);
    ddq_max = 1*ones(kin.KinPar.NJ,1);
    [traj.PosPath,traj.PosPathVelocity]=trajKunz(q_path,dq_max,ddq_max);

    %Step 2: Check each robot configuration statically
    sampling = round(size(traj.PosPath,1)/50); % check only 50 points
    for i=1:sampling:size(traj.PosPath,1)
        [Statics]=StaticCheck_pnt(traj.PosPath(i,:)', kin, task);
        if (Statics==1)
            traj.success=1;
        else
            traj.success=0;
        break
        end
    end
end