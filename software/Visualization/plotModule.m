function plotModule(Module)
%%% plot joint/link module with input and output coordinate system

figure;hold on;light
%% plot 3D-Model
if Module.Mod.typ == 1 % joint module
    
% proximal
if ~isempty(Module.Kpl.shape)
    T = get_transformMatrix(Module);
    shape = invert_T(T)*padarray(Module.Kpl.shape,1,1,'post');
    x = shape(1,:)';
    y = shape(2,:)';
    z = shape(3,:)';
    k = convhulln(shape(1:3,:)');
    trisurf(k,x,y,z,'Facecolor','cyan','FaceLighting','gouraud','EdgeColor','none','LineStyle','none','FaceAlpha',0.5);
end

% distal
if ~isempty(Module.Kdl.shape)
    x = Module.Kdl.shape(1,:)';
    y = Module.Kdl.shape(2,:)';
    z = Module.Kdl.shape(3,:)';
    k = convhulln(Module.Kdl.shape');
    trisurf(k,x,y,z,'Facecolor','cyan','FaceLighting','gouraud','EdgeColor','none','LineStyle','none','FaceAlpha',0.5);
end

else
% plot convex hull of vertices
if ~isempty(Module.Kpl.shape)
    x = Module.Kpl.shape(1,:)';
    y = Module.Kpl.shape(2,:)';
    z = Module.Kpl.shape(3,:)';
    k = convhulln(Module.Kpl.shape');
    trisurf(k,x,y,z,'Facecolor','cyan','FaceLighting','gouraud','EdgeColor','none','LineStyle','none','FaceAlpha',0.5);
end
end

axis equal
view([1,1,1]);
%% plot output coordinate system
plotCS(eye(4),'out');

%% plot input coordinate system
T = get_transformMatrix(Module);
plotCS(invert_T(T),'in');

%% plot DH

end
%% plot coordinate system
function plotCS(T,name)
c = T(1:3,4);
x_axis = T*[0.05;0;0;1];
y_axis = T*[0;0.05;0;1];
z_axis = T*[0;0;0.05;1];

mArrow3(c,x_axis(1:3),'color','red');
mArrow3(c,y_axis(1:3),'color','green');
mArrow3(c,z_axis(1:3),'color','blue');

text(x_axis(1),x_axis(2),x_axis(3),['x_{',name,'}'],'color','red');
text(y_axis(1),y_axis(2),y_axis(3),['y_{',name,'}'],'color','green');
text(z_axis(1),z_axis(2),z_axis(3),['z_{',name,'}'],'color','blue');
end

function T = get_transformMatrix(module)

% Case: Link module
if module.Mod.typ ~= 1
    p         = module.Kpl.p_pl;
    a         = module.Kpl.a_pl;
    n         = module.Kpl.n_pl;
    alpha     = module.Kpl.alpha_pl;
    delta_in  = module.Kpl.delta_pl;
    delta_out = module.Kdl.delta_dl;
    % Get trafo Matrix
    T = Ro('z',-delta_in)*Tr('z',-p)*Tr('x',a)...
        *Ro('x',alpha)*Tr('z',n)*Ro('z',delta_out);
else
    % Case: Joint module
        
    % Joint module is first of this robot link
    p2     = module.Kdl.p_dl;
    a2     = module.Kdl.a_dl;
    n2    = module.Kdl.n_dl;
    alpha2     = module.Kdl.alpha_dl;
    delta_out = module.Kdl.delta_dl;
    
    p1     = module.Kpl.p_pl;
    a1     = module.Kpl.a_pl;
    n1     = module.Kpl.n_pl;
    alpha1 = module.Kpl.alpha_pl;
    delta_in = module.Kpl.delta_pl;
    
    delta_j = module.Kj.delta_j;
    
    % Transformation Matrix
    T = Ro('z',-delta_in)*Tr('z',-p1)*Tr('x',a1)...
        *Ro('x',alpha1)*Tr('z',n1)*Ro('z',delta_j)*Tr('z',-p2)*Tr('x',a2)...
        *Ro('x',alpha2)*Tr('z',n2)*Ro('z',delta_out);
end


end