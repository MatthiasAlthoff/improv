%% Paths to include
% Install:
% - Modular Robotics:

%% Parameters
%  ModIDSeq = [1; 14; 9; 15; 2; 5; 3; 0];% 8-DOF without EE
ModIDSeq = [1; 4; 2; 4; 9; 7; 5; 3; 0];% 8-DOF without EE
% ModIDSeq = [1; 4; 2; 5; 3; 12];    % config n3, classical robot with ee
% ModIDSeq = [1; 4; 2; 6; 7; 8 ; 5; 3; 12];    % 6-DOF, with long extension
% ModIDSeq = [1; 4; 8; 2; 7; 5; 3; 12];    % config n4, medium
ModRob = ModID2ModRob(ModIDSeq);

% define q_sim (timeseries object)
load('8dof_noEE') 
% load('6dof_classicalEE')

%% Animate
% modes; 'vertices', 'capsules', 'both'
mode = 3;
plotModRob(q_sim,ModRob,'vertices');
