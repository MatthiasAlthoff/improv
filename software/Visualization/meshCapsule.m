function [x1,y1,z1,x2,y2,z2,xc,yc,zc] = meshCapsule(capsule)

p1 = capsule(1:3);
p2 = capsule(4:6);
r = capsule(7);

[x,y,z] = sphere;
[xc,yc,zc] = cylinder(r);
zc = norm(p2-p1)*(zc-0.5); % cylinder heigth

%cylinder rotation
if norm(p2-p1)>0.000001
    aa = vrrotvec((p2-p1)./norm(p2-p1),[0;0;1]);
    R = vrrotvec2mat(aa)';
    for i=1:2
        c1 = [xc(i,:);yc(i,:);zc(i,:)];
        c1 = R*c1;
        xc(i,:) = c1(1,:);
        yc(i,:)=  c1(2,:);
        zc(i,:)=  c1(3,:);
    end
end

%cylinder translation
xc = xc+(p2(1)+p1(1))/2;
yc = yc+(p2(2)+p1(2))/2;
zc = zc+(p2(3)+p1(3))/2;

x1 = r*x+p1(1);
y1 = r*y+p1(2);
z1 = r*z+p1(3); % centered at p1

x2 = r*x+p2(1);
y2 = r*y+p2(2);
z2 = r*z+p2(3); % centered at p1

end

