function [g]=get_g(q,KinPar,DynPar)
% Numerical or symbolical calculation of the gravity vector of a manipulator 
% using Newton-Euler-Algorithm (fixed base and revolute joints)
%
%  Input:       q       (current position)
%               KinPar  (Robot model, kinematic parameters)
%               DynPar  (Robot model, dynamic parameters)
%               
%  Output:      g       (gravity vector)
%
% Author:       Stefan Liu
% Written:      03-Nov-2015
% Last update:  23-May-2018
% Last revision:---

N = KinPar.NJ;
zeroN = zeros(N,1);
g_base = 9.81;

% Call Newton-Euler-Algorithm
g = NE_mod(q, zeroN, zeroN, zeroN, zeroN,KinPar,DynPar,g_base);


end