function [C]=get_C(q,dq,KinPar,DynPar)
% Numerical or symbolical calculation of the Skew-symmetric Coriolis Matrix of a manipulator
% using Newton-Euler-Algorithm (fixed base and revolute joints)
%
%  Input:       q       (joint position)
%               dq      (joint velocity)
%               KinPar  (Robot model, kinematic parameters)
%               DynPar  (Robot model, dynamic parameters)
%
%  Output:      C       (Coriolis Matrix)
%
% Author:       Stefan Liu
% Written:      03-Nov-2015
% Last update:  21-Nov-2016
% Last revision:---

N = KinPar.NJ;
Base = eye(N);

% Preallocate Space
C = zeros(N);
zeroN = zeros(N,1);

% set gravity vector to zero
g_base = 0;

% NE for every column of M
for i = 1:N
    C(:,i) = NE_mod(q, dq, Base(:,i), zeroN,zeroN,KinPar,DynPar,g_base);
end