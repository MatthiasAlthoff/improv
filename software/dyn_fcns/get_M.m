function [M]=get_M(q,KinPar,DynPar)
% Numerical or symbolical calculation of the Mass matrix of a manipulator
% using Newton-Euler-Algorithm (fixed base and revolute joints)
%
%  Input:       q       (current position)
%               KinPar  (Robot model, kinematic parameters)
%               DynPar  (Robot model, dynamic parameters)
%
%  Output:      M       (Mass matrix)
%
% Author:       Stefan Liu
% Written:      03-Nov-2015
% Last update:  23-May-2018
% Last revision:---

N = KinPar.NJ;
Base = eye(N);

% Preallocate Space
M = zeros(N);
zeroN = zeros(N,1);


% set gravity vector to zero
g = 0;

% NE for every column of M
for i = 1:N
    M(:,i) = NE_mod(q, zeroN, zeroN, zeroN,Base(:,i),KinPar,DynPar,g);
end

end