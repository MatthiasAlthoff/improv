% main_test - Script for testing functions
% Syntax:  
%
% Inputs: 
%
% Example: 
%
% Other m-files required: none
% Subfunctions: none
% MAT-files required: none
%
% See also: 

% Author:       Stefan Liu
% Written:      08-11-2018
% Last update:  
% Last revision:---

%------------- BEGIN CODE --------------

%clc;
%close all;
%clear;

% Add folders to the path
folder = fileparts(which(mfilename)); 
addpath(genpath(folder));

init_KukaLWR4pDB('modulesKukaLWR4p'); % Initialize the DB of modules of the Schunk LWA4P arm

% Initialize the DB of modules of new Modular Robot
load('modulesKukaLWR4p.mat');
load('Gripper.mat');
ModRob = [LWR4p_0;LWR4p_1; LWR4p_2; LWR4p_3; LWR4p_4; LWR4p_5; LWR4p_6;LWR4p_7;SchunkPg70]; % 8-DOF no End effector


%% Robot Assembly Schunk
% Schunk assemblies
% ModRob = [PB1; L8; PB4; L9; PB2; L2; PB3]; % 8-DOF no End effector
% ModRob = [PB1; L1; PB2; L2; PB3; L3]; % 6-DOF with End effector
% ModIDSeq = [1,4,2,5,3,12];
% ModRob = [PB1; L1];

% % 6-DOF with End effector (IDs)
% ModIDSeq = [1,4,2,5,3,12];
% ModRob = ModID2ModRob(ModIDSeq,'modulesSchunkLWA4P.mat');
% 
% %% Synthesis of kin. parameters
% % get extended DH table and pose of the base frame from ModRob
% [DHext,B]   = ModRob2DHext(ModRob);
% % get standard DHtab from DHext
% [DHtab, jt] = DHext2DH(DHext);
% 
% %% Synthesis of dyn. parameters
% DynPar = ModRob2Dynpar(ModRob,DHext);
% 
% %% Synthesis of collision model
% EncRepresent = 'capsules'; %vertices OR capsules
% Enclosures = generateCollisionModel(ModRob,EncRepresent);
% 
% %% Call numerically the modified recursive N-E algorithm 
% 
% KinPar.B     = B(1:3,1:3);
% KinPar.DHext = DHext;
% KinPar.NJ    = get_NJ(ModRob);
% gravity      = -9.81; 
% 
% q   = zeros(KinPar.NJ,1);
% dq  = zeros(KinPar.NJ,1);
% dqA = zeros(KinPar.NJ,1);
% ddq = zeros(KinPar.NJ,1);
% 
% % compute inverse dynamics
% tau = NE_mod(q,dq,dqA,dq,ddq,KinPar,DynPar,gravity);
% 
% % compute forward kinematics of collision model
% robotOccupancies = fwdkinCollisionModel(q,DHext,Enclosures,EncRepresent);
% 
% %Visualize one robot pose
% q_pose = [1;1;1;1;1;1;1;1];
% plotModRob(q_pose,ModRob,'vertices')
% 
% %Visualize trajectory (timeseries)
% % load('6dof_classicalEE')
% load('8dof_noEE')

plotModRob(zeros(6,1),ModRob,'vertices')


