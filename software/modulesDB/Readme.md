How to add new (non-modular) robot (*=robot name) to modular robot toolbox:

1) Create new folder for robot
2) Create new Robot init-file 'init_*.m' , e.g., copy and rename KukaLWR4p/init_KukaLWR4pDB.m
3) In 'init_*.m'-file, insert the robot kinematic parameters using DH-Convention: 
    - a -> Kdl.a_dl, 
    - d -> -Kdl.p_dl, 
    - alpha -> Kdl.alpha_dl, 
    - theta -> Kj.delta_J
4) In 'init_*.m'-file, insert the robot dynamic parameters must be relative to DH-Frame: 
    - m -> Ddl.m, 
    - r_com -> Ddl.rcom_dl, 
    - I -> Ddl.I_dl
    - do not forget to add joint-limits, torque limits, gear ratio, friction etc. into 'Kj' and 'Dj'

For the collision model

5) Create new folder 'STL files'
6) Create an STL file for each robot link. The DH-Frame must be the coordinate frame of the STL files!
7) use 'Kdl.shape = importStl('stlFilename');' in 'init_*.m'-file to import STL for each link
8) create a test file 'test_*.m' in unitTests/ and check whether all files have been imported correctly.