% Add a new field to module DB: shapes (zonotopes)


%% Joint Modules

%PB1
Z = cylinderPointCloud(0.045,0.045); %in part
T = Tr('z',0.045);
Z = rigidTransformPointCloud(Z,T);
PB1(1).Kpl.shape = Z; %in part
PB1(1).Kdl.shape = [];

Z = spherePointCloud(0.09);
T = Tr('z',0.075-0.1013);
Z = rigidTransformPointCloud(Z,T);
PB1(2).Kpl.shape = Z; % Bigball
PB1(2).Kdl.shape = cylinderPointCloud(0.045,0.045); %out part

%PB2
Z = cylinderPointCloud(0.045,0.045); %in part
T = Tr('z',0.045);
Z = rigidTransformPointCloud(Z,T);
PB2(1).Kpl.shape = Z;%in part
PB2(1).Kdl.shape = [];


Z = spherePointCloud(0.09);
T = Tr('z',0.075-0.1013);
Z = rigidTransformPointCloud(Z,T);
PB2(2).Kpl.shape = Z; % Bigball
PB2(2).Kdl.shape = cylinderPointCloud(0.045,0.045); %out part


%PB3 %%%%CHECK!
Z = cylinderPointCloud(0.035,0.04); %in part
T = Tr('z',0.04);
Z = rigidTransformPointCloud(Z,T);
PB3(1).Kpl.shape = Z; %in part
PB3(1).Kdl.shape = [];


Z = spherePointCloud(0.07);
T = Tr('z',0.055-0.0748);
Z = rigidTransformPointCloud(Z,T);
PB3(2).Kpl.shape = Z; % Bigball
PB3(2).Kdl.shape = cylinderPointCloud(0.0475,0.055); %out part


%PB4
Z = cylinderPointCloud(0.045,0.045); %in part
T = Tr('z',0.045);
Z = rigidTransformPointCloud(Z,T);
PB4(1).Kpl.shape = Z; %in part
PB4(1).Kdl.shape = [];

Z = spherePointCloud(0.09);
T = Tr('z',0.075-0.1013);
Z = rigidTransformPointCloud(Z,T);
PB4(2).Kpl.shape = Z; % Bigball
PB4(2).Kdl.shape = cylinderPointCloud(0.045,0.045); %out part

%% Link modules

%L1 Long link
% Z = rectanglePointCloud(0.11,0.46,0.125);
% Tz = Tr('z',0.02);
% Ty = Tr('y',-0.175);
% Z = rigidTransformPointCloud(Z,Tz*Ty);
% L1.Kpl.shape = Z;
L1.Kpl.shape=importStl('L1.stl');
L1.Kdl.shape = [];


%L2 Short link
% Z = rectanglePointCloud(0.10,0.27,0.17);
% Tz = Tr('z',0.065+0.0748);
% Ty = Tr('y',-0.085);
% Z = rigidTransformPointCloud(Z,Tz*Ty);
% L2.Kpl.shape = Z;
L2.Kpl.shape=importStl('L2.stl');
L2.Kdl.shape = [];


%L3 End effector (gripper)
% Z = rectanglePointCloud(0.085,0.115,0.15);
% Tz = Tr('z',+0.15-0.08);
% Z = rigidTransformPointCloud(Z,Tz);
% L3.Kpl.shape = Z;
L3.Kpl.shape=importStl('L3.stl');
L3.Kdl.shape = [];


%L4 link extension short
L4.Kpl.shape = cylinderPointCloud(0.035,0.064);
L4.Kdl.shape = [];


%L5 link extension medium
L5.Kpl.shape = cylinderPointCloud(0.035,0.084);
L5.Kdl.shape = [];

%L6 link extension long
L6.Kpl.shape = cylinderPointCloud(0.035,0.104);
L6.Kdl.shape = [];


%L7 End effector 2
Z = rectanglePointCloud(0.085,0.115,0.15);
Tz = Tr('z',+0.15-0.24);
Z = rigidTransformPointCloud(Z,Tz);
L7.Kpl.shape = Z;
L7.Kdl.shape = [];

%L8 Long link
% Z = rectanglePointCloud(0.11,0.31,0.09); % 0.15 m shorter than original Long link
% Tz = Tr('z',0.02);
% Ty = Tr('y',-0.100);
% Z = rigidTransformPointCloud(Z,Tz*Ty);
L8.Kpl.shape=importStl('L8.stl');
% L8.Kpl.shape = Z;
L8.Kdl.shape = [];


%L9 Short link
% Z = rectanglePointCloud(0.10,0.19,0.17); % 0.08 m shorter than original short link
% Tz = Tr('z',0.065+0.0748);
% Ty = Tr('y',-0.05);
% Z = rigidTransformPointCloud(Z,Tz*Ty);
L9.Kpl.shape=importStl('L9.stl');
% L9.Kpl.shape = Z;
L9.Kdl.shape = [];

clearvars T Ty Tz Tx Z
