function init_StaubliTxDB(output_filename)

if ~exist('output_filename','var')
    output_filename = 'modules.mat';
end

m1 = 45;
m2 = 28;
m3 = 10;
m4 = 6;
m5 = 5;
m6 = 3;

c1 = [-0.0310;    0.0650;    0.0250];
c2 = [-0.2520;  0; -0.1710];
c3 = [0;  0;  0.02];
c4 = [0; 0;    0.2];
c5 = [0; 0; 0];
c6 = [0;  0;  -0.05];

Ic1 = inertia(0.806,0.146,  -0.009, 0.489, 0.012, 0.787);
Ic2 = inertia(0.941,    0,  -1.207, 3.521, 0, 2.75);
Ic3 = inertia(0.0100, 1.1700, 0, 4.8100, 0, 2.7500);
Ic4 = inertia(3.0900, -0.4800, 0, 0.7400, 0, 0.8800);
Ic5 = inertia(0.0100, 0, 0, 0.0100, 0.3200, 1.4300);
Ic6 = inertia(0.0100, 0, 0, 0.0100, 0,     0.0100);
Io1 = Ic1;% + m1 * S(c1)' * S(c1);
Io2 = Ic2;% + m2 * S(c2)' * S(c2);
Io3 = Ic3;% + m3 * S(c3)' * S(c3);
Io4 = Ic4;% + m4 * S(c4)' * S(c4);
Io5 = Ic5;% + m5 * S(c5)' * S(c5);
Io6 = Ic6;% + m6 * S(c6)' * S(c6);

Im1 = 4.5200;
Im2 = 0.1100;
Im3 = 1.8800;
Im4 = 1.5000;
Im5 = 1.5000;
Im6 = 1.5000;

Fv1 = 68.9600;
Fv2 = 72.6800;
Fv3 = 15.2700;
Fv4 = 10.0000;
Fv5 = 6.7700;
Fv6 = 3.3700;

Fc1 = 21.3700;
Fc2 = 11.1;
Fc3 = 5.8000;
Fc4 = 4.7000;
Fc5 = 4.7100;
Fc6 = 5.0500;

TX90_1.Mod = struct('ID',   1,   'typ',      1,          'Cplx',    2,           'CANidTX', 515,    'CANidRX',  387);
TX90_1.Kpl = struct('a_pl', 0,   'alpha_pl', 0,          'p_pl',    0,           'n_pl',    0,      'delta_pl', 0);
TX90_1.Kdl = struct('a_dl', 0.0500,   'alpha_dl', -pi/2,       'p_dl',-0.478,           'n_dl',    0,      'delta_dl', 0);
TX90_1.Kj  = struct('jt',   0,   'delta_j',  0,          'Ujl',     pi,          'Ljl',     -pi);
TX90_1.Dpl = struct('m_pl', 0,   'I_pl',     zeros(3,3), 'rcom_pl', zeros(3,1));
TX90_1.Ddl = struct('m_dl', m1,  'I_dl',     Io1,        'rcom_dl', c1);
TX90_1.Dj  = struct('Im',   Im1, 'jbv',      Fv1,        'jbc',     Fc1,         'k_tau',   1,      'k_r',      1, 'tau_lim', 3000, 'curr_lim', 3000000, 'dq_lim', 0, 'ddq_lim', 0);

TX90_2.Mod = struct('ID',   2,   'typ',      1,          'Cplx',    2,           'CANidTX', 516,    'CANidRX',  388);
TX90_2.Kpl = struct('a_pl', 0,   'alpha_pl', 0,          'p_pl',    0,           'n_pl',    0,      'delta_pl', 0);
TX90_2.Kdl = struct('a_dl', 0.4250,   'alpha_dl', pi,      'p_dl', -0.05,           'n_dl',    0,      'delta_dl', 0);
TX90_2.Kj  = struct('jt',   0,   'delta_j' , -pi/2,          'Ujl',     pi,          'Ljl',     -pi);
TX90_2.Dpl = struct('m_pl', 0,   'I_pl',     zeros(3,3), 'rcom_pl', zeros(3,1));
TX90_2.Ddl = struct('m_dl', m2,  'I_dl',     Io2,        'rcom_dl', c2);
TX90_2.Dj  = struct('Im',   Im2, 'jbv',      Fv2,        'jbc',     Fc2,         'k_tau',   1,      'k_r',      1, 'tau_lim', 3000, 'curr_lim', 3000000, 'dq_lim', 0, 'ddq_lim', 0);

TX90_3.Mod = struct('ID',   3,   'typ',      1,          'Cplx',    2,           'CANidTX', 517,    'CANidRX',  389);
TX90_3.Kpl = struct('a_pl', 0,   'alpha_pl', 0,          'p_pl',    0,           'n_pl',    0,      'delta_pl', 0);
TX90_3.Kdl = struct('a_dl', 0,   'alpha_dl', -pi/2,      'p_dl',    0,        'n_dl',    0,      'delta_dl', 0);
TX90_3.Kj  = struct('jt',   0,   'delta_j',  -pi/2,          'Ujl',     pi,          'Ljl',     -pi);
TX90_3.Dpl = struct('m_pl', 0,   'I_pl',     zeros(3,3), 'rcom_pl', zeros(3,1));
TX90_3.Ddl = struct('m_dl', m3,  'I_dl',     Io3,        'rcom_dl', c3);
TX90_3.Dj  = struct('Im',   Im3, 'jbv',      Fv3,        'jbc',     Fc3,         'k_tau',   1,      'k_r',      1, 'tau_lim', 3000, 'curr_lim', 3000000, 'dq_lim', 0, 'ddq_lim', 0);

TX90_4.Mod = struct('ID',   4,   'typ',      1,          'Cplx',    2,           'CANidTX', 518,    'CANidRX',  390);
TX90_4.Kpl = struct('a_pl', 0,   'alpha_pl', 0,          'p_pl',    0,           'n_pl',    0,      'delta_pl', 0);
TX90_4.Kdl = struct('a_dl', 0,   'alpha_dl', -pi/2,       'p_dl', -0.425,           'n_dl',    0,      'delta_dl', 0);
TX90_4.Kj  = struct('jt',   0,   'delta_j',  0,          'Ujl',     pi,          'Ljl',     -pi);
TX90_4.Dpl = struct('m_pl', 0,   'I_pl',     zeros(3,3), 'rcom_pl', zeros(3,1));
TX90_4.Ddl = struct('m_dl', m4,  'I_dl',     Io4,        'rcom_dl', c4);
TX90_4.Dj  = struct('Im',   Im4, 'jbv',      Fv4,        'jbc',     Fc4,         'k_tau',   1,      'k_r',      1, 'tau_lim', 3000, 'curr_lim', 3000000, 'dq_lim', 0, 'ddq_lim', 0);

TX90_5.Mod = struct('ID',   5,   'typ',      1,          'Cplx',    2,           'CANidTX', 519,    'CANidRX',  391);
TX90_5.Kpl = struct('a_pl', 0,   'alpha_pl', 0,          'p_pl',    0,           'n_pl',    0,      'delta_pl', 0);
TX90_5.Kdl = struct('a_dl', 0,   'alpha_dl', pi/2,       'p_dl',    0,        'n_dl',    0,      'delta_dl', 0);
TX90_5.Kj  = struct('jt',   0,   'delta_j',  0,          'Ujl',     pi,          'Ljl',     -pi);
TX90_5.Dpl = struct('m_pl', 0,   'I_pl',     zeros(3,3), 'rcom_pl', zeros(3,1));
TX90_5.Ddl = struct('m_dl', m5,  'I_dl',     Io5,        'rcom_dl', c5);
TX90_5.Dj  = struct('Im',   Im5, 'jbv',      Fv5,        'jbc',     Fc5,         'k_tau',   1,      'k_r',      1, 'tau_lim', 3000, 'curr_lim', 3000000,  'dq_lim', 0, 'ddq_lim', 0);

TX90_6.Mod = struct('ID',   6,   'typ',      1,          'Cplx',    2,           'CANidTX', 520,    'CANidRX',  392);
TX90_6.Kpl = struct('a_pl', 0,   'alpha_pl', 0,          'p_pl',    0,           'n_pl',    0,      'delta_pl', 0);
TX90_6.Kdl = struct('a_dl', 0,   'alpha_dl', 0,        'p_dl',    -0.09,           'n_dl',    0,      'delta_dl', 0);
TX90_6.Kj  = struct('jt',   0,   'delta_j' , 0,          'Ujl',     pi,          'Ljl',     -pi);
TX90_6.Dpl = struct('m_pl', 0,   'I_pl',     zeros(3,3), 'rcom_pl', zeros(3,1));
TX90_6.Ddl = struct('m_dl', m6,  'I_dl',     Io6,        'rcom_dl', c6);
TX90_6.Dj  = struct('Im',   Im6, 'jbv',      Fv6,        'jbc',     Fc6,         'k_tau',   1,      'k_r',      1, 'tau_lim', 3000, 'curr_lim', 3000000,  'dq_lim', 0, 'ddq_lim', 0);


TX90_1.Kdl.shape=importStl('TX90_1.stl');
TX90_1.Kpl.shape = [];
TX90_2.Kdl.shape=importStl('TX90_2.stl');
TX90_2.Kpl.shape = [];
TX90_3.Kdl.shape=importStl('TX90_3.stl');
TX90_3.Kpl.shape = [];
TX90_4.Kdl.shape=importStl('TX90_4.stl');
TX90_4.Kpl.shape = [];
TX90_5.Kdl.shape=importStl('TX90_5.stl');
TX90_5.Kpl.shape = [];
TX90_6.Kdl.shape=importStl('TX90_6.stl');
TX90_6.Kpl.shape = [];

clear m1 m2 m3 m4 m5 m6

clear c1 c2 c3 c4 c5 c6  

clear Ic1 Ic2 Ic3 Ic4 Ic5 Ic6 

clear Io1 Io2 Io3 Io4 Io5 Io6 

clear Im1 Im2 Im3 Im4 Im5 Im6 

clear Fv1 Fv2 Fv3 Fv4 Fv5 Fv6 

clear Fc1 Fc2 Fc3 Fc4 Fc5 Fc6 

%save every variable in the workspace except the 'output_filename' and
%'Module' variables
variables = who;
toexclude = {'output_filename', 'Module'};
variables = variables(~ismember(variables, toexclude));
save(output_filename, variables{:})

end

function I = inertia(xx, xy, xz, yy, yz, zz)
I = [xx, xy, xz;
     xy, yy, yz;
     xz, yz, zz];
end
