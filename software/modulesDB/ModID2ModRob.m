function ModRob = ModID2ModRob(ModIDSeq,modulesFilename)
persistent S
persistent filename
if nargin < 2
    modulesFilename = 'modules.mat';
end
if isempty(S)
    S = load(modulesFilename);
end
if isempty(filename)
    filename = modulesFilename;
end
if ~strcmp(filename,modulesFilename)
    S = load(modulesFilename);
end


ModList = cell2mat(struct2cell(S));

% pointer to the location of ModRob
pntr = 1;
% iterate far all the IDs sequence 
for i=1:length(ModIDSeq)
    % iterate for all the module info available
    for k=1:length(ModList)
        % when the ID matches it is inserted in the location pointed 
        if  (ModList(k).Mod.ID == ModIDSeq(i) && ModIDSeq(i)~=0)
            ModRob(pntr) = ModList(k);
            pntr = pntr+1;
        end
    end
end


end