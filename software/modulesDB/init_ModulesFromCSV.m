%% Script for saving the structs from a csv module database to a file
% Syntax:  
%
% Inputs: 
%   - database: Module Database csv-file for Modular Robot 
%       (default: 'ModuleDB.csv')
%   - output_file: Filename for new .mat file where module structs are saved
%       (default: 'modules.mat')
% Example: 
%   - init_ModRobDB() - load from and save in default files
%   - init_ModRobDB('DB_ModRob2.csv') - load from 'DB_ModRob2.csv' and save
%   in default file
%   - init_ModRobDB('DB_ModRob3.csv', 'new_modules.mat') load from 'DB_ModRob3.csv' and save
%   in 'new_modules.mat' 
%
% Other m-files required: none
% Subfunctions: none
% MAT-files required: none
% Other files required: ModuleDB.csv
%
% See also: 

% Author:       Hanna Krasowski, Alexander Zhu
% Written:      05-05-2018
% Last update:  16-05-2018
%               20-06-2018 (Stefan) new import routine for STL-files
% Last revision:---

%------------- BEGIN CODE --------------
function init_ModulesFromCSV(filename, output_filename)

%% Initialize variables.
if ~exist('filename','var')
    filename = 'ModuleDB.csv';
end

if ~exist('output_filename','var')
    output_filename = 'modules.mat';
end

delimiter = ';';
startRow = 2;

%% Format for each line of text:
%   column1: text (%s)
%	column2 - column57: double (%f)

formatSpec = '%s%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%[^\n\r]';

%% Open the text file.
fileID = fopen(filename,'r');

%% Read columns of data according to the format.
% This call is based on the structure of the file used to generate this
% code. If an error occurs for a different file, try regenerating the code
% from the Import Tool.
dataArray = textscan(fileID, formatSpec, 'Delimiter', delimiter, 'TextType', 'string', 'HeaderLines' ,startRow-1, 'ReturnOnError', false, 'EndOfLine', '\r\n');

%% Close the text file.
fclose(fileID);


%% Create output variable
DBModRob = table(dataArray{1:end-1}, 'VariableNames', {'Name','ID','typ','Cplx','CANidTX','CANidRX','a_pl','alpha_pl','p_pl','n_pl','delta_pl','a_dl','alpha_dl','p_dl','n_dl','delta_dl','jt','delta_j','Ujl','Ljl','m_pl','I_pl','VarName23','VarName24','VarName25','VarName26','VarName27','VarName28','VarName29','VarName30','r_com_pl','VarName32','VarName33','m_dl','I_dl','VarName36','VarName37','VarName38','VarName39','VarName40','VarName41','VarName42','VarName43','r_com_dl','VarName45','VarName46','Im','jbc','jbv','k_tau','k_r','tau_lim','curr_lim','dq_lim','ddq_lim','length','d_out'});

nRow = size(DBModRob,1);
nCol = size(DBModRob,2);


%% find all module name cells of the DB and store them in the Mod(#modules,name_of_module) matrix

%NOTE: This procedure needs to be revised for modules with DOF > 1
m=1;
ModList=cell(1,1);

for i_row= 1:nRow
    if isstring(DBModRob(i_row,1))~=1
        ModList{m,1}=i_row;
        ModList{m,2}=table2cell(DBModRob(i_row,1));
        m=m+1;
    end
        
end

%% for each module - build a struct of the following format:
%init module struct prototype
% Module.Mod = struct('ID'  ,0,'typ',0,'Cplx',0,'CANidTX',0,'CANidRX',0,'Input_size',0,'Output_size',0);
% Module.Kpl = struct('a_pl',0,'alpha_pl',0,'p_pl',0,'n_pl',0,'delta_pl',0);
% Module.Kdl = struct('a_dl',0,'alpha_dl',0,'p_dl',0,'n_dl',0,'delta_dl',0);
% Module.Kj  = struct( 'jt' ,0,'delta_j' ,0,'Ujl' ,0,'Ljl' ,0); 
% Module.Dpl = struct('m_pl',0,'I_pl' ,zeros(3,3),'rcom_pl',zeros(3,1));
% Module.Ddl = struct('m_dl',0,'I_dl' ,zeros(3,3),'rcom_dl',zeros(3,1));
% Module.Dj  = struct('Im'  ,0,'jbv',0,'jbc',0,'k_tau',0,'k_r',0,'tau_lim',0,'curr_lim',0,'dq_lim',0,'ddq_lim',0);

% Module = proto_module_ModRob(); - function creates empty struct
[M,~]=size(ModList);

for m_i=1:M
    Module = proto_module();
    Module.Mod = struct('ID',table2array(DBModRob(m_i,2)),'typ',table2array(DBModRob(m_i,3)),'Cplx',table2array(DBModRob(m_i,4)),'CANidTX',table2array(DBModRob(m_i,5)),'CANidRX',table2array(DBModRob(m_i,6)),'InputConnectorSize',table2array(DBModRob(m_i,56)),'OutputConnectorSize',table2array(DBModRob(m_i,57)));
    Module.Kpl = struct('a_pl',table2array(DBModRob(m_i,7)),'alpha_pl',table2array(DBModRob(m_i,8)),'p_pl',table2array(DBModRob(m_i,9)),'n_pl',table2array(DBModRob(m_i,10)),'delta_pl',table2array(DBModRob(m_i,11)));
    Module.Kdl = struct('a_dl',table2array(DBModRob(m_i,12)),'alpha_dl',table2array(DBModRob(m_i,13)),'p_dl',table2array(DBModRob(m_i,14)),'n_dl',table2array(DBModRob(m_i,15)),'delta_dl',table2array(DBModRob(m_i,16)));
    Module.Kj  = struct( 'jt' ,table2array(DBModRob(m_i,17)),'delta_j' ,table2array(DBModRob(m_i,18)),'Ujl' ,table2array(DBModRob(m_i,19)),'Ljl' ,table2array(DBModRob(m_i,20))); 
    Module.Dpl = struct('m_pl',table2array(DBModRob(m_i,21)),'I_pl' ,[table2array(DBModRob(m_i,22)),table2array(DBModRob(m_i,23)),table2array(DBModRob(m_i,24));table2array(DBModRob(m_i,25)),table2array(DBModRob(m_i,26)),table2array(DBModRob(m_i,27));table2array(DBModRob(m_i,28)),table2array(DBModRob(m_i,29)),table2array(DBModRob(m_i,30))],'rcom_pl',[table2array(DBModRob(m_i,31));table2array(DBModRob(m_i,32));table2array(DBModRob(m_i,33))]);
    Module.Ddl = struct('m_dl',table2array(DBModRob(m_i,34)),'I_dl' ,[table2array(DBModRob(m_i,35)),table2array(DBModRob(m_i,36)),table2array(DBModRob(m_i,37));table2array(DBModRob(m_i,38)),table2array(DBModRob(m_i,39)),table2array(DBModRob(m_i,40));table2array(DBModRob(m_i,41)),table2array(DBModRob(m_i,42)),table2array(DBModRob(m_i,43))],'rcom_dl',[table2array(DBModRob(m_i,44));table2array(DBModRob(m_i,45));table2array(DBModRob(m_i,46))]);
    Module.Dj  = struct('Im'  ,table2array(DBModRob(m_i,47)),'jbv',table2array(DBModRob(m_i,48)),'jbc',table2array(DBModRob(m_i,49)),'k_tau',table2array(DBModRob(m_i,50)),'k_r',table2array(DBModRob(m_i,51)),'tau_lim',table2array(DBModRob(m_i,52)),'curr_lim',table2array(DBModRob(m_i,53)),'dq_lim',table2array(DBModRob(m_i,54)),'ddq_lim',table2array(DBModRob(m_i,55)));
    name=char(table2array(DBModRob(m_i,1)));
    name(name==' ') = '_';
    name = matlab.lang.makeValidName(name);
    DB.(name) = Module;
    clear Module;
end

Module = proto_module();
%% Convert the module fields of the DB struct to seperate structs
command=structvars(DB);
for i=1:M
    eval(command(i,:));
end
%% Add module shapes (field element in Kpl/Kdl)
fields = fieldnames(DB);
for i = 1:numel(fields)
    stlFilename = [fields{i},'.STL'];
    if exist(stlFilename,'file') && DB.(fields{i}).Mod.typ ~= 1 % Case: Link/Endeffector/Base module
        eval([fields{i},'.Kpl.shape=importStl(stlFilename);']); % import link shape
    elseif DB.(fields{i}).Mod.typ == 1 % Case: Joint module
        stlFilename_p = [fields{i},'_p.STL']; %proximal part of joint
        stlFilename_d = [fields{i},'_d.STL']; %distal part of joint
        if exist(stlFilename_p,'file')
            eval([fields{i},'.Kpl.shape=importStl(stlFilename_p);']); %import proximal part
        end
        if exist(stlFilename_d,'file')
            eval([fields{i},'.Kdl.shape=importStl(stlFilename_d);']); %import distal part
        end
    end
end

% correct delta-in delta out
correct_modules;

% manually defined shapes
init_ModRob_shapes;

%% Clear temporary variables
clearvars filename delimiter startRow formatSpec fileID dataArray nRow nCol name ModList m_i M m i_row DBModRob ans i DB command fields stlFilename stlFilename_d stlFilename_p;

%save every variable in the workspace except the 'output_filename' and
%'Module' variables
variables = who;
toexclude = {'output_filename', 'Module'};
variables = variables(~ismember(variables, toexclude));
save(output_filename, variables{:})
end