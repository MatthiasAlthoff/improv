function init_KukaLWR4pDB(output_filename)

if ~exist('output_filename','var')
    output_filename = 'modules.mat';
end

m1 = 4.1948152162;
m2 = 4.2996847737;
m3 = 3.658530333;
m4 = 2.3846673548;
m5 = 1.7035567183;
m6 = 0.4000713156;
m7 = 0.6501439811;

c1 = [-0.0216387515; 0.1;           -0.0376881829];
c2 = [0.0003284751;  -0.0041132249; 0.0823647642];
c3 = [0.0002593328;  0.1137431845;  -0.000100257];
c4 = [-0.0014648843; -0.0000461;    0.148580959];
c5 = [-0.0003791484; -0.0553526131; -0.0101255137];
c6 = [0.0020739022;  0.0586184696;  -0.044799983];
c7 = [-0.0004601303; 0.0014789221;  0.0715608282];

Ic1 = inertia(0.01,         0.01,         0.01,               0.0018932828,  0.01,               0.01);
Ic2 = inertia(0.0474108647, 0.05,         0.001601901,        -0.00000621,   0.0001166457,       -0.0009141575);
Ic3 = inertia(0.0469510749, 0.0008344566, 0.05,               -0.000271431,  4.09E-008,          -0.000577228);
Ic4 = inertia(0.0124233226, 0.0072708907, 0.0099884782,       -0.0005187982, 0.000000225,        -0.0005484476);
Ic5 = inertia(0.006322648,  0.0012020203, 0.0070806218,       -0.0002163196, 0.00000652,         -0.005);
Ic6 = inertia(0.0005278646, 0,            0.0034899625,       0.0000483,     -0.0000375,         -0.0010605344);
Ic7 = inertia(0,            -0.000000577, c7(1) * c7(3) * m7, 0.0000323,     c7(2) * c7(3) * m7, 0.0001187527);

Io1 = Ic1 + m1 * Skew(c1)' * Skew(c1);
Io2 = Ic2 + m2 * Skew(c2)' * Skew(c2);
Io3 = Ic3 + m3 * Skew(c3)' * Skew(c3);
Io4 = Ic4 + m4 * Skew(c4)' * Skew(c4);
Io5 = Ic5 + m5 * Skew(c5)' * Skew(c5);
Io6 = Ic6 + m6 * Skew(c6)' * Skew(c6);
Io7 = Ic7 + m7 * Skew(c7)' * Skew(c7);

Im1 = 3.20;
Im2 = 3.05;
Im3 = 1.98;
Im4 = 2.06;
Im5 = 0.801;
Im6 = 0.391;
Im7 = 0.399;

Fv1 = 14.4;
Fv2 = 15.3;
Fv3 = 6.51;
Fv4 = 11.0;
Fv5 = 4.37;
Fv6 = 2.25;
Fv7 = 1.6;

Fc1 = 11.6;
Fc2 = 11.1;
Fc3 = 8.60;
Fc4 = 8.02;
Fc5 = 7.38;
Fc6 = 4.67;
Fc7 = 6.04;

% Base
LWR4p_0.Mod = struct('ID',   10,   'typ',      2,          'Cplx',    1,           'CANidTX', 515,    'CANidRX',  387);
LWR4p_0.Kpl = struct('a_pl', 0,   'alpha_pl', 0,          'p_pl',    -0.1,           'n_pl',    0,      'delta_pl', 0);
LWR4p_0.Kdl = struct('a_dl', 0,   'alpha_dl', 0,          'p_dl',    0,           'n_dl',    0,      'delta_dl', 0);
LWR4p_0.Kj  = struct('jt',   0,   'delta_j',  0,          'Ujl',     0,           'Ljl',     0);
LWR4p_0.Dpl = struct('m_pl', 0,   'I_pl',     zeros(3,3), 'rcom_pl', zeros(3,1));
LWR4p_0.Ddl = struct('m_dl', m1,  'I_dl',     Io1,        'rcom_dl', c1);
LWR4p_0.Dj  = struct('Im',   0,   'jbv',      0,          'jbc',       0,         'k_tau',   0,      'k_r',      0, 'tau_lim', 0, 'curr_lim', 0, 'dq_lim', 0, 'ddq_lim', 0);

LWR4p_1.Mod = struct('ID',   1,   'typ',      1,          'Cplx',    1,           'CANidTX', 515,    'CANidRX',  387);
LWR4p_1.Kpl = struct('a_pl', 0,   'alpha_pl', 0,          'p_pl',    0,           'n_pl',    0,      'delta_pl', 0);
LWR4p_1.Kdl = struct('a_dl', 0,   'alpha_dl', pi/2,       'p_dl',    -0.2,           'n_dl',    0,      'delta_dl', 0);
LWR4p_1.Kj  = struct('jt',   0,   'delta_j',  0,          'Ujl',     pi,          'Ljl',     -pi);
LWR4p_1.Dpl = struct('m_pl', 0,   'I_pl',     zeros(3,3), 'rcom_pl', zeros(3,1));
LWR4p_1.Ddl = struct('m_dl', m1,  'I_dl',     Io1,        'rcom_dl', c1);
LWR4p_1.Dj  = struct('Im',   Im1, 'jbv',      Fv1,        'jbc',     Fc1,         'k_tau',   1,      'k_r',      1, 'tau_lim', 176, 'curr_lim', 176, 'dq_lim', 0, 'ddq_lim', 0);

LWR4p_2.Mod = struct('ID',   2,   'typ',      1,          'Cplx',    1,           'CANidTX', 516,    'CANidRX',  388);
LWR4p_2.Kpl = struct('a_pl', 0,   'alpha_pl', 0,          'p_pl',    0,           'n_pl',    0,      'delta_pl', 0);
LWR4p_2.Kdl = struct('a_dl', 0,   'alpha_dl', -pi/2,      'p_dl',    0,           'n_dl',    0,      'delta_dl', 0);
LWR4p_2.Kj  = struct('jt',   0,   'delta_j' , 0,          'Ujl',     pi,          'Ljl',     -pi);
LWR4p_2.Dpl = struct('m_pl', 0,   'I_pl',     zeros(3,3), 'rcom_pl', zeros(3,1));
LWR4p_2.Ddl = struct('m_dl', m2,  'I_dl',     Io2,        'rcom_dl', c2);
LWR4p_2.Dj  = struct('Im',   Im2, 'jbv',      Fv2,        'jbc',     Fc2,         'k_tau',   1,      'k_r',      1, 'tau_lim', 176, 'curr_lim', 176000, 'dq_lim', 0, 'ddq_lim', 0);

LWR4p_3.Mod = struct('ID',   3,   'typ',      1,          'Cplx',    1,           'CANidTX', 517,    'CANidRX',  389);
LWR4p_3.Kpl = struct('a_pl', 0,   'alpha_pl', 0,          'p_pl',    0,           'n_pl',    0,      'delta_pl', 0);
LWR4p_3.Kdl = struct('a_dl', 0,   'alpha_dl', -pi/2,      'p_dl',    -0.40,        'n_dl',    0,      'delta_dl', 0);
LWR4p_3.Kj  = struct('jt',   0,   'delta_j',  0,          'Ujl',     pi,          'Ljl',     -pi);
LWR4p_3.Dpl = struct('m_pl', 0,   'I_pl',     zeros(3,3), 'rcom_pl', zeros(3,1));
LWR4p_3.Ddl = struct('m_dl', m3,  'I_dl',     Io3,        'rcom_dl', c3);
LWR4p_3.Dj  = struct('Im',   Im3, 'jbv',      Fv3,        'jbc',     Fc3,         'k_tau',   1,      'k_r',      1, 'tau_lim', 100, 'curr_lim', 100000, 'dq_lim', 0, 'ddq_lim', 0);

LWR4p_4.Mod = struct('ID',   4,   'typ',      1,          'Cplx',    1,           'CANidTX', 518,    'CANidRX',  390);
LWR4p_4.Kpl = struct('a_pl', 0,   'alpha_pl', 0,          'p_pl',    0,           'n_pl',    0,      'delta_pl', 0);
LWR4p_4.Kdl = struct('a_dl', 0,   'alpha_dl', pi/2,       'p_dl',    0,           'n_dl',    0,      'delta_dl', 0);
LWR4p_4.Kj  = struct('jt',   0,   'delta_j',  0,          'Ujl',     pi,          'Ljl',     -pi);
LWR4p_4.Dpl = struct('m_pl', 0,   'I_pl',     zeros(3,3), 'rcom_pl', zeros(3,1));
LWR4p_4.Ddl = struct('m_dl', m4,  'I_dl',     Io4,        'rcom_dl', c4);
LWR4p_4.Dj  = struct('Im',   Im4, 'jbv',      Fv4,        'jbc',     Fc4,         'k_tau',   1,      'k_r',      1, 'tau_lim', 100, 'curr_lim', 100000, 'dq_lim', 0, 'ddq_lim', 0);

LWR4p_5.Mod = struct('ID',   5,   'typ',      1,          'Cplx',    1,           'CANidTX', 519,    'CANidRX',  391);
LWR4p_5.Kpl = struct('a_pl', 0,   'alpha_pl', 0,          'p_pl',    0,           'n_pl',    0,      'delta_pl', 0);
LWR4p_5.Kdl = struct('a_dl', 0,   'alpha_dl', pi/2,       'p_dl',    -0.39,        'n_dl',    0,      'delta_dl', 0);
LWR4p_5.Kj  = struct('jt',   0,   'delta_j',  0,          'Ujl',     pi,          'Ljl',     -pi);
LWR4p_5.Dpl = struct('m_pl', 0,   'I_pl',     zeros(3,3), 'rcom_pl', zeros(3,1));
LWR4p_5.Ddl = struct('m_dl', m5,  'I_dl',     Io5,        'rcom_dl', c5);
LWR4p_5.Dj  = struct('Im',   Im5, 'jbv',      Fv5,        'jbc',     Fc5,         'k_tau',   1,      'k_r',      1, 'tau_lim', 100, 'curr_lim', 100000,  'dq_lim', 0, 'ddq_lim', 0);

LWR4p_6.Mod = struct('ID',   6,   'typ',      1,          'Cplx',    1,           'CANidTX', 520,    'CANidRX',  392);
LWR4p_6.Kpl = struct('a_pl', 0,   'alpha_pl', 0,          'p_pl',    0,           'n_pl',    0,      'delta_pl', 0);
LWR4p_6.Kdl = struct('a_dl', 0,   'alpha_dl', -pi/2,      'p_dl',    0,           'n_dl',    0,      'delta_dl', 0);
LWR4p_6.Kj  = struct('jt',   0,   'delta_j' , 0,          'Ujl',     pi,          'Ljl',     -pi);
LWR4p_6.Dpl = struct('m_pl', 0,   'I_pl',     zeros(3,3), 'rcom_pl', zeros(3,1));
LWR4p_6.Ddl = struct('m_dl', m6,  'I_dl',     Io6,        'rcom_dl', c6);
LWR4p_6.Dj  = struct('Im',   Im6, 'jbv',      Fv6,        'jbc',     Fc6,         'k_tau',   1,      'k_r',      1, 'tau_lim', 30, 'curr_lim', 30000,  'dq_lim', 0, 'ddq_lim', 0);

LWR4p_7.Mod = struct('ID',   7,   'typ',      1,          'Cplx',    1,           'CANidTX', 521,    'CANidRX',  393);
LWR4p_7.Kpl = struct('a_pl', 0,   'alpha_pl', 0,          'p_pl',    0,           'n_pl',    0,      'delta_pl', 0);
LWR4p_7.Kdl = struct('a_dl', 0,   'alpha_dl', 0,          'p_dl',    -0.085,           'n_dl',    0,      'delta_dl', 0);
LWR4p_7.Kj  = struct('jt',   0,   'delta_j',  0,          'Ujl',     pi,          'Ljl',     -pi);
LWR4p_7.Dpl = struct('m_pl', 0,   'I_pl',     zeros(3,3), 'rcom_pl', zeros(3,1));
LWR4p_7.Ddl = struct('m_dl', m7,  'I_dl',     Io7,        'rcom_dl', c7);
LWR4p_7.Dj  = struct('Im',   Im7, 'jbv',      Fv7,        'jbc',     Fc7,         'k_tau',   1,      'k_r',      1, 'tau_lim', 30, 'curr_lim', 30000, 'dq_lim', 0, 'ddq_lim', 0);

LWR4p_0.Kpl.shape = importStl('LWR4p_0.stl');
LWR4p_1.Kdl.shape=importStl('LWR4p_1.stl');
LWR4p_1.Kpl.shape = [];
LWR4p_2.Kdl.shape=importStl('LWR4p_2.stl');
LWR4p_2.Kpl.shape = [];
LWR4p_3.Kdl.shape=importStl('LWR4p_3.stl');
LWR4p_3.Kpl.shape = [];
LWR4p_4.Kdl.shape=importStl('LWR4p_4.stl');
LWR4p_4.Kpl.shape = [];
LWR4p_5.Kdl.shape=importStl('LWR4p_5.stl');
LWR4p_5.Kpl.shape = [];
LWR4p_6.Kdl.shape=importStl('LWR4p_6.stl');
LWR4p_6.Kpl.shape = [];
LWR4p_7.Kdl.shape=importStl('LWR4p_7.stl');
LWR4p_7.Kpl.shape = [];

clear m1 m2 m3 m4 m5 m6 m7 

clear c1 c2 c3 c4 c5 c6 c7 

clear Ic1 Ic2 Ic3 Ic4 Ic5 Ic6 Ic7

clear Io1 Io2 Io3 Io4 Io5 Io6 Io7

clear Im1 Im2 Im3 Im4 Im5 Im6 Im7

clear Fv1 Fv2 Fv3 Fv4 Fv5 Fv6 Fv7

clear Fc1 Fc2 Fc3 Fc4 Fc5 Fc6 Fc7

%save every variable in the workspace except the 'output_filename' and
%'Module' variables
variables = who;
toexclude = {'output_filename', 'Module'};
variables = variables(~ismember(variables, toexclude));
save(output_filename, variables{:})

end

function I = inertia(xx, xy, xz, yy, yz, zz)
I = [xx, xy, xz;
     xy, yy, yz;
     xz, yz, zz];
end
