% assembleLinkModules - attaches multiple link modules and to create an
% assembled link module
%
% Syntax:  
%    assembleLinkModules(ModRob)
%
% Inputs: 
%    ModRob -> Array, containing only link modules
%    ID .   -> Desired Module ID
% Outputs:
%    module -> assembled link module
%
% Example: 
%
% Other m-files required: none       
% Subfunctions: none
% MAT-files required: none
%
% See also: 

% Author:       Stefan Liu
% Written:      25-October-2018
% Last update:  ---
% Last revision:---

%------------- BEGIN CODE --------------

function [module] = assembleLinkMods(ModRob,ID)

[ND,ModRob] = get_ND(ModRob); % Number of Modules (a joint module with n axes is decomposed into n joint modules)

%% Kinematics and transformation matrices
T = zeros(4,4,ND+1);
T(:,:,1) = eye(4);
for i=1:ND
    % get transformation matrix
    T_i = get_transformMatrixAndGeometry(ModRob(i));
    T(:,:,i+1) = T(:,:,i)*T_i;
end
% Module parameters
[alpha,a,n,p,delta_in,delta_out] = Trafo2ModDH(T(:,:,end)); 

%% Dynamics
m = 0;
I = zeros(3,3);
r_com = zeros(3,1);
for i=1:ND
    % Module Dynamics
    m_pl_i     = ModRob(i).Dpl.m_pl;
    I_pl_i     = ModRob(i).Dpl.I_pl;
    r_pl_com_i = ModRob(i).Dpl.rcom_pl;
    
    % Transform to Frame of 1st module
    T_i = T(:,:,i);
    R_i = T_i(1:3,1:3);
    r_com_i = T_i*[r_pl_com_i;1];
    r_com_i = r_com_i(1:3);
    I_i = R_i*(I_pl_i-m_pl_i*Skew(r_pl_com_i)'*Skew(r_pl_com_i))*R_i'+m_pl_i*Skew(r_com_i)'*Skew(r_com_i);
    
    % Assemble
    m = m+m_pl_i;
    r_com = ((m-m_pl_i)*r_com + m_pl_i*r_com_i)/m;
    I = I+I_i;
end

%% Collision model
V = ModRob(1).Kpl.shape; %vertices
for i=2:ND
    T_i = get_transformMatrixAndGeometry(ModRob(i)); % get transformation matrix
    V_i = ModRob(i).Kpl.shape; % get pointCloud
    
    % transform previous pointClouds to current frame
    V = rigidTransformPointCloud(V,invert_T(T_i));
    
    % add current module geometry to point cloud
    V = [V,V_i];
end


% Fill in
module = proto_module();
module.Dj = rmfield(module.Dj,'Ke');
module.Mod.ID = ID;

module.Kpl.a_pl = a;
module.Kpl.alpha_pl = alpha;
module.Kpl.p_pl = p;
module.Kpl.n_pl = n;
module.Kpl.delta_pl = delta_in;
module.Kdl.delta_dl = delta_out;

module.Dpl.m_pl = m;
module.Dpl.rcom_pl = r_com;
module.Dpl.I_pl = I;

module.Kpl.shape = V;
end

% Function: Number of Modules (a joint module with n axes is decomposed into n joint modules)
function [ND,ModRob_m] = get_ND(ModRob)
ND =  0;
mods = [];
for k =1:length(ModRob)
    if ModRob(k).Mod.ID ~= 0 % empty module
        ND = ND+1;
        mods = [mods,k];
    end
end
ModRob_m = ModRob(mods);
end

% Function: Get Modular Transformation matrix
function [T] = get_transformMatrixAndGeometry(module)

% Case: Link module
p         = module.Kpl.p_pl;
a         = module.Kpl.a_pl;
n         = module.Kpl.n_pl;
alpha     = module.Kpl.alpha_pl;
delta_in  = module.Kpl.delta_pl;
delta_out = module.Kdl.delta_dl;

% Transformation Matrix
% T = Ro('z',-delta_in)*Tr('z',-p)*Tr('x',a)...
%     *Ro('x',alpha)*Tr('z',n)*Ro('z',delta_out);
sin_alpha = sin(alpha);
cos_alpha = cos(alpha);
sin_delta_in = sin(delta_in);
cos_delta_in = cos(delta_in);
sin_delta_out = sin(delta_out);
cos_delta_out = cos(delta_out);
T = zeros(4,4);
T(1,:)=[ cos_delta_in*cos_delta_out + cos_alpha*sin_delta_in*sin_delta_out, cos_alpha*cos_delta_out*sin_delta_in - cos_delta_in*sin_delta_out, -sin_alpha*sin_delta_in,   a*cos_delta_in - n*sin_alpha*sin_delta_in];
T(2,:)=[ cos_alpha*cos_delta_in*sin_delta_out - cos_delta_out*sin_delta_in, sin_delta_in*sin_delta_out + cos_alpha*cos_delta_in*cos_delta_out, -cos_delta_in*sin_alpha, - a*sin_delta_in - cos_delta_in*n*sin_alpha];
T(3,:)=[                                           sin_alpha*sin_delta_out,                                           cos_delta_out*sin_alpha,               cos_alpha,                             cos_alpha*n - p];
T(4,:)=[                                                                 0,                                                                 0,                       0,                                           1];

end

