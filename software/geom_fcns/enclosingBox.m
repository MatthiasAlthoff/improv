function [box] = enclosingBox(V)
% Compute overapproximate enclosures of robot links given a modular robot
%
% Syntax:  
%
% Inputs:
%    V - vertices in 3D
%
% Outputs:
%    capsule - capsule, that tightly encloses the vertices [p1;p2;r]
%
% Other m-files required:
% Subfunctions: none
% MAT-files required: none
%

% Author:       Stefan Liu
% Written:      27-October-2017 
% Last update:  ---
% Last revision: ---

%------------- BEGIN CODE --------------
if isempty(V)
    box = [];
    return
end

% Get min. bounding box
x = V(1,:)';
y = V(2,:)';
z = V(3,:)';
[R,cornerpoints] = minboundbox(x,y,z,'v',1);


box = cornerpoints';

end

