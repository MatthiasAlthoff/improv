function [V_out] = rigidTransformPointCloud(V,T)
% Perform a rigid transformation of a pointCloud in 3d space 
%
% Syntax:  
%    Vout = rigidTransformPointCloud(Z,T)
%
% Inputs:
%    V - 3xN Point cloud with N points
%    T - 4x4 Transformation matrix
%
% Outputs:
%    V_out - transformed pointCloud
%
% Other m-files required:
% Subfunctions: none
% MAT-files required: none
%

% Author:       Stefan Liu
% Written:      06-June-2017 
% Last update:  ---
% Last revision: ---

%------------- BEGIN CODE --------------
if isempty(V)
    V_out = [];
else
    V_aux = [V;ones(1,size(V,2))]; % pad with ones
    V_out = T*V_aux;
    V_out = V_out(1:3,:);
end
end

