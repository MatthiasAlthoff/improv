function [Vout] = cylinderPointCloud(radius,height)

% Circle in x,y
N=30;
s = linspace(0,2*pi,N+1);
s(end) = [];
circlePoints = radius.*[cos(s);sin(s);zeros(1,N)];

% Circle at z=0 AND Circle at z=-height
Vout = [circlePoints,circlePoints+repmat([0;0;-height],1,N)];

end


