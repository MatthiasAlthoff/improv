function [ptcloud] = stl2pointcloud(filename)
%stl2pointcloud Imorts and then converts an STL file into an pointcloud
%data object
% 
%Input: filename   ->    path to the stl file

%Output: ptcloud    ->   point cloud object

%Dependencies: Computer Vision Toolbox; stlread

STL = stlread(filename);

ptcloud = pointCloud(STL.vertices);

%% Show pointcloud in figure
%pcshow(ptcloud);

%% Plot origin for orientation
% hold on;
% plot3([0,0.01],[0,0],[0,0],'b','LineWidth',2)
% hold on;
% plot3([0,0],[0,0.01],[0,0],'g','LineWidth',2)
% hold on;
% plot3([0,0],[0,0],[0,0.01],'r','LineWidth',2)
% hold on;

end


