function [Vout] = rectanglePointCloud(length,width,height)
% start of z is on surface, rectangle is on negative half of z axis

Vout = [length/2, -length/2, length/2, -length/2, length/2, -length/2, length/2, -length/2;...
         width/2,   width/2, -width/2,  -width/2,  width/2,   width/2, -width/2,  -width/2;...
               0,         0,        0,         0,  -height,   -height,  -height,   -height];
  
end
