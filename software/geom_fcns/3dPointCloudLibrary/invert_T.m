function Tout = invert_T(T)
% Invert rigid transformation matrix
%
% Syntax:  
%    Tout = invertRigidTransform(T)
%
% Inputs:
%    T       rigid Transformation matrix 4x4

% Outputs:
%    Tout    inverted trafo 4x4
%
% Other m-files required: none
% Subfunctions: none
% MAT-files required: none
%

% Author:       Stefan Liu
% Written:      25-October-2017 
% Last update:  ---
% Last revision: ---

%------------- BEGIN CODE --------------

R = T(1:3,1:3);
t = T(1:3,4);

Tout = [R',-R'*t;...
        0 0 0 1 ];
end