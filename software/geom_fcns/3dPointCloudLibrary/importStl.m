function [V] = importStl(filename)
%stl2pointcloud Imorts and then converts an STL file into an pointcloud
%data object
% 
%Input: filename   ->    path to the stl file

%Output: ptcloud    ->   point cloud object

%Dependencies: Computer Vision Toolbox; stlread

[ptcloud] = stl2pointcloud(filename);
K = unique(convhull(ptcloud.Location(:,1),ptcloud.Location(:,2),ptcloud.Location(:,3)));
V = ptcloud.Location(K,:)';

end
