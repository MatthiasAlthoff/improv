function [Vout] = spherePointCloud(radius)
% Generate pointCloud of sphere

[X,Y,Z] = sphere(16); %n-by-n sphere
Vout = radius.*[ X(:)'; Y(:)'; Z(:)'] - repmat([0;0;radius],1,length(X(:)'));

end

