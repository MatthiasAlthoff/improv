function [linkEncl,V] = generateCollisionModel(ModRob,representation)
% Compute overapproximate enclosures of robot robot links given a modular robot
%
% Syntax:
%
% Inputs:
%    ModRob - Struct containing Parameters of the modular robot
%    representation - 'zonotope','vertices','capsules'
%
% Outputs:
%    linkEncl - cell array 1 x N + 1 containing overapproximative link
%    enclosures from link 0 until link N
%
% Other m-files required:
% Subfunctions: none
% MAT-files required: none
%

% Author:       Stefan Liu
% Written:      25-October-2017
% Last update:  02-November-2017 (ModRob-Shapes/capsules are now matrices)
%               06-June-2018 (restructured, no dependency on MPT)

% Last revision: ---

%------------- BEGIN CODE --------------

%% Number of robot links
NJ = get_NJ(ModRob); %Number of joints
NA = NJ + 1; % Number of robot links
[ND,ModRob] = get_ND(ModRob); % Number of Modules (a joint module with n axes is decomposed into n joint modules)

%% Group Modules into robot links (step 2)
modules_of_link = group_modules(ModRob,NA,ND);

%% Assemble links: Transform all point clouds into DHext-Frame of robot link i (step 3)
V = cell(1,NA);
for i=1:NA
    j = 1;
    V_i = [];
    T = eye(4);
    while j<=length(modules_of_link{i}) % iterate through all modules
        % get transformation matrix
        [T_j,V_i_j] = get_transformMatrixAndGeometry(modules_of_link{i}(j),i,j);
        T = T*T_j;
        
        % transform previous pointClouds to current frame
        V_i = rigidTransformPointCloud(V_i,invert_T(T_j));
        
        % add current module geometry to point cloud
        V_i = [V_i,V_i_j];
        
        j=j+1;
    end
    
    % finally, align x-Axis to DHext-Convention
    phi = get_phi(T);
    V{i} = rigidTransformPointCloud(V_i,Ro('z', -phi));
end

%% Generate efficient representation (step 4)
linkEncl = cell(1,NA);
for i=1:NA
    switch representation
%         case 'zonotope'
%             linkEncl{i} = zonotope(vertices(V{i}));
        case 'vertices'
            linkEncl{i} = V{i};
        case 'capsules'
            linkEncl{i} = enclosingCapsule(V{i});
        case 'box'
            linkEncl{i} = enclosingBox(V{i});
        otherwise
            linkEncl{i} = enclosingCapsule(V{i}); %default
    end
end

if strcmp(representation,'capsules')
    linkEnclosuresCmat = zeros(7,NJ);
    for i=1:NA
        linkEnclosuresCmat(:,i) = linkEncl{i};
    end
    linkEncl = linkEnclosuresCmat;
end

end


%% Subfunctions

% Function: Number of Modules (a joint module with n axes is decomposed into n joint modules)
function [ND,ModRob_m] = get_ND(ModRob)
ND =  0;
mods = [];
for k =1:length(ModRob)
    if ModRob(k).Mod.ID ~= 0 % empty module
        ND = ND+1;
        mods = [mods,k];
    end
end
ModRob_m = ModRob(mods);
end

% Function: Group Modules into robot links
function modules_of_link = group_modules(ModRob,NA,ND)
modules_of_link = cell(1,NA);
j = 1;
% fill in for each robot link
for i = 1:NA
    while ModRob(j).Mod.typ ~= 1 && j < ND %if nor joint module or end effector
        modules_of_link{i} = [modules_of_link{i},ModRob(j)];
        j = j+1;
    end
    modules_of_link{i} = [modules_of_link{i},ModRob(j)]; %joint module or endeffector
    if j<ND %joint module is also part of the next robot link except if endeffector
        modules_of_link{i+1} = [modules_of_link{i+1},ModRob(j)];
        j = j+1;
    end
end
end

% Function: Get Modular Transformation matrix
function [T,V] = get_transformMatrixAndGeometry(module,armNo,modNo)

% Case: Link module
if module.Mod.typ ~= 1
    p         = module.Kpl.p_pl;
    a         = module.Kpl.a_pl;
    n         = module.Kpl.n_pl;
    alpha     = module.Kpl.alpha_pl;
    delta_in  = module.Kpl.delta_pl;
    delta_out = module.Kdl.delta_dl;
    V = module.Kpl.shape;  % Geometry as pointCloud
else
    % Case: Joint module
        
    % Joint module is first of this robot link
    if modNo == 1 && armNo ~= 1
        p     = module.Kdl.p_dl;
        a     = module.Kdl.a_dl;
        n     = module.Kdl.n_dl;
        alpha     = module.Kdl.alpha_dl;
        delta_in = 0;
        delta_out = module.Kdl.delta_dl;
        V = module.Kdl.shape;  % Geometry as pointCloud   
        
    % Joint module is last of this robot link OR
    % Joint module is first of the entire robot (proximal part is base)
    else
        p     = module.Kpl.p_pl;
        a     = module.Kpl.a_pl;
        n     = module.Kpl.n_pl;
        alpha = module.Kpl.alpha_pl;
        delta_in = module.Kpl.delta_pl;
        delta_out = 0;
        V = module.Kpl.shape;  % Geometry as pointCloud
    end
end

% Transformation Matrix
T = Ro('z',-delta_in)*Tr('z',-p)*Tr('x',a)...
    *Ro('x',alpha)*Tr('z',n)*Ro('z',delta_out);

end

% Function: Get phi such that Transformation T*Ro('phi') aligns with DHext-Transformation
function phi = get_phi(F_aux)
zim1 = [0 0 1]';
zi = F_aux(1:3,3);
pxyz_im1 = F_aux(1:3,4);
Rim1_i = F_aux(1:3,1:3);
% to avoid numerical problems we introduce a guard
guard = 1e-3;
% init impact that a delta has on the next module
%     phi = 0;
% Case 1: zim1 and zi are overlapped
if (norm(cross(zi,zim1)) < guard) && (norm(pxyz_im1(1:2)) < guard)
    phi = 0;
    % Case 2: zim1 and zi are parallel
elseif norm(cross(zi,zim1)) < guard
    % Be careful, the angle is expressed in frame im1 but the
    % rotation must be performed in frame i, so that the
    % angle in i must be determined.
    pxyz_i = transpose(Rim1_i)*pxyz_im1;
    phi = atan2(pxyz_i(2),pxyz_i(1));
    % Case 3: zim1 and zi are skew or intersect
else
    nx_im1 = abs(cross(zi,zim1));
    nx_i = transpose(Rim1_i)*nx_im1;
    phi = atan2(nx_i(2),nx_i(1));
end
end