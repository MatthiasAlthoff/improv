function [capsule] = enclosingCapsule(V)
% Compute overapproximate enclosures of robot links given a modular robot
%
% Syntax:  
%
% Inputs:
%    V - vertices in 3D
%
% Outputs:
%    capsule - capsule, that tightly encloses the vertices [p1;p2;r]
%
% Other m-files required:
% Subfunctions: none
% MAT-files required: none
%

% Author:       Stefan Liu
% Written:      27-October-2017 
% Last update:  ---
% Last revision: ---

%------------- BEGIN CODE --------------
if isempty(V)
    capsule = zeros(7,1);
    return
end

if isequal(V,zeros(3,1))
    capsule = zeros(7,1);
    return
end

% Get min. bounding box
x = V(1,:)';
y = V(2,:)';
z = V(3,:)';

%slow
% [R,cornerpoints] = minboundbox(x,y,z,'v',1);
% center = median(cornerpoints)';
% [max_length,index] = max(max((R'*cornerpoints')')-min((R'*cornerpoints')')); %direction of max length
% direction = zeros(3,1);
% direction(index) = 1;
% direction = R*direction;


%fast
R = pca(V');
center = ((max(V')+min(V'))/2)';
direction = R(:,1);
k = V'*direction/(direction'*direction); % project points into direction
max_length = max(k) - min(k);


% largest distance to axis
center_repmat = repmat(center,1,size(V,2));
distances = sqrt(sum(((center_repmat-V)-repmat(((center_repmat-V)'*direction)',3,1).*repmat(direction,1,size(V,2))).^2, 1));
distance_max = max(distances);

% minimal radius sphere at center
p1_min = center;
p2_min = center;
r_min = max(sqrt(sum((V-center_repmat).^2, 1))); %minimal radius
volume_min = 4/3*pi*r_min^3;

% minimize volume of capsule
for i=0.005:0.005:max_length/2
    p2 = center + i*direction; %point 2 of capsule
    p1 = center - i*direction; %point 1 of capsule

    % split vertices
    V2 = V(:,direction'*(V-repmat(p2,1,size(V,2))) >= 0); %part after p2
    V1 = V(:,direction'*(V-repmat(p1,1,size(V,2))) < 0); %part behind p1
    
    r_i1 = max(sqrt(sum((V1-repmat(p1,1,size(V1,2))).^2, 1))); %minimal radius
    r_i2 = max(sqrt(sum((V2-repmat(p2,1,size(V2,2))).^2, 1))); %minimal radius
    r_i = max([r_i1,r_i2,distance_max]); %at least distance_max
    
    volume = 4/3*pi*r_i^3 + r_i^2*pi*norm(p2-p1);
    if volume < volume_min
        % capsule_min = ...capsule parameters
        p1_min = p1;
        p2_min = p2;
        r_min = r_i;
        volume_min=volume;
    end
end

capsule = [p1_min;p2_min;r_min];

end

