function [robotOccupancies] = fwdkinCollisionModel(q,DHext,Enclosures,mode)
% Get robot occupancy in Cartesian Space depending on its joint positions
%
% Syntax:
%
% Inputs:
%    q - current joint positions
%    mode - 'zonotopes', 'vertices' or 'capsules'
%
% Outputs:
%    robotCapsules - Occupancy in Cartesian space (zonotopes/vertices) of each robot link
%
% Other m-files required:
% Subfunctions: none
% MAT-files required: none
%

% Author:       Stefan Liu
% Written:      25-October-2017
% Last update:  ---
% Last revision: ---

%------------- BEGIN CODE --------------
NA =  size(Enclosures,2);

robotOccupancies = Enclosures;
for i=1:NA
    [p,R] = fwdkin(q(1:i-1),DHext(1:i-1,:),zeros(size(DHext(1:i-1,:),1),1)); % forward kinematics
    T = [R,p;0 0 0 1];
    switch mode
        case {'vertices','box'}
            if isempty(Enclosures{i})
                robotOccupancies{i} = [];
            else
                V = T*[Enclosures{i} ; ones(1,size(Enclosures{i},2))];
                robotOccupancies{i} = V(1:3,:);
            end
        case 'capsules'
            p1 = T*[Enclosures(1:3,i);1];
            p2 = T*[Enclosures(4:6,i);1];
            r = Enclosures(7,i);
            robotOccupancies(:,i) = [p1(1:3);p2(1:3);r];
    end
end

