function plotModRob(q_sim,ModRob,mode,task)
% Get robot occupancy in Cartesian Space depending on its joint positions
%
% Syntax:
%
% Inputs:
%    ModIDSeq - Array containing the robot's configuration
%    q_sim - trajectory as a timeseries object
%    mode - 'vertices', 'capsules', 'box', 'both'(vertices and capsules)
%
% Other m-files required:
% createModRob,linkEnclosures,meshCapsule,minboundbox,robotOccupancy
% Subfunctions:
% MAT-files required:


% Author:       Stefan Liu
% Written:      02-May-2017
% Last update:  ---
% Last revision: ---

%------------- BEGIN CODE --------------
if strcmp(mode,'capsules')
    modus = 2;
elseif strcmp(mode,'vertices')
    modus = 1;
elseif strcmp(mode,'box')
    modus = 3;
elseif strcmp(mode,'both')
    modus = 4;
else %both
    error('invalid mode, can only be capsules, vertices, or box, or both (vertices and capsules)');
end

% Compute Enclosures
switch modus
    case 1
        EnclosuresV = generateCollisionModel(ModRob,'vertices');
%         % downsample vertices to 500 points only
%         for i=1:length(EnclosuresV)
%             if size(EnclosuresV{i},2)>500
%                 k = randperm(size(EnclosuresV{i},2));
%                 EnclosuresV{i} = EnclosuresV{i}(:,k(1:500));
%             end
%         end     
    case 2
        EnclosuresC = generateCollisionModel(ModRob,'capsules');
    case 3
        EnclosuresB = generateCollisionModel(ModRob,'box');
    case 4
        EnclosuresV = generateCollisionModel(ModRob,'vertices');
        EnclosuresC = generateCollisionModel(ModRob,'capsules');
end


% Get DHext
DHext = ModRob2DHext(ModRob);
[DHtab, jt] = DHext2DH(DHext);

%% initialize figure
N = get_NJ(ModRob);
if isa(q_sim,'timeseries')
    q = q_sim.Data(1,:)'; %q0
else %numeric
    q = q_sim(:,1);
end

% Initialize enclosures at q0
switch modus
    case 1
        robotOccupanciesV = fwdkinCollisionModel(q,DHext,EnclosuresV,'vertices');
    case 2
        robotOccupanciesC = fwdkinCollisionModel(q,DHext,EnclosuresC,'capsules');
    case 3
        robotOccupanciesB = fwdkinCollisionModel(q,DHext,EnclosuresB,'box');
    case 4
        robotOccupanciesV = fwdkinCollisionModel(q,DHext,EnclosuresV,'vertices');
        robotOccupanciesC = fwdkinCollisionModel(q,DHext,EnclosuresC,'capsules');
end
% robotOccupanciesC = fwdkinCollisionModel(q,DHext,EnclosuresC,'capsules');
% robotOccupanciesV = fwdkinCollisionModel(q,DHext,EnclosuresV,'vertices');
% robotOccupanciesB = fwdkinCollisionModel(q,DHext,EnclosuresB,'box');

f=figure;hold on;light

% Plot obstacle
if nargin>3
    obstacle = task.obstacle;
    [x1,y1,z1,x2,y2,z2,xc,yc,zc] = meshCapsule(obstacle(1,[1:3,1:3,4]));
    surf(x1,y1,z1,'Facecolor','red','EdgeColor','none','LineStyle','none','FaceAlpha',0.3);
    surf(x2,y2,z2,'Facecolor','red','EdgeColor','none','LineStyle','none','FaceAlpha',0.3);
    surf(xc,yc,zc,'Facecolor','red','EdgeColor','none','LineStyle','none','FaceAlpha',0.3);
end

% txt = ['Time:   ','0.2',' s'];
% plot_text = text(0.5,0.5,0.5,txt,'HorizontalAlignment','left','FontSize',8);

for j=1:N+1
    % plot convex hull of vertices
    if modus == 1 || modus == 4 
        if ~isempty(robotOccupanciesV{j})
            x = robotOccupanciesV{j}(1,:)';
            y = robotOccupanciesV{j}(2,:)';
            z = robotOccupanciesV{j}(3,:)';
            k = convhulln(robotOccupanciesV{j}');
            s(j)=trisurf(k,x,y,z,'Facecolor','cyan','FaceLighting','gouraud','EdgeColor','none','LineStyle','none');
        end
    end
    
    % plot boxes
    if modus == 3
        if ~isempty(robotOccupanciesB{j})
            x = robotOccupanciesB{j}(1,:)';
            y = robotOccupanciesB{j}(2,:)';
            z = robotOccupanciesB{j}(3,:)';
            k = convhulln(robotOccupanciesB{j}');
            s(j)=trisurf(k,x,y,z,'Facecolor','cyan','FaceLighting','gouraud','EdgeColor','none','LineStyle','none');
        end
    end
    
    % plot capsules
    if modus == 2 || modus == 4
        [x1,y1,z1,x2,y2,z2,xc,yc,zc] = meshCapsule(robotOccupanciesC(:,j));
        s1(j)=surf(x1,y1,z1,'Facecolor','red','EdgeColor','none','LineStyle','none','FaceAlpha',0.3);
        s2(j)=surf(x2,y2,z2,'Facecolor','red','EdgeColor','none','LineStyle','none','FaceAlpha',0.3);
        sc(j)=surf(xc,yc,zc,'Facecolor','red','EdgeColor','none','LineStyle','none','FaceAlpha',0.3);
    end
end
ax = gca;
xlim([-0.4 0.6]);
ylim([-1 1]);
zlim([0 1.5]);
ax.XLimMode = 'manual';
ax.YLimMode = 'manual';
ax.ZLimMode = 'manual';

% 
% axis([-0.5 0.8 -1 1 0 1.5]);hold on;
xlabel('x')
ylabel('y')
zlabel('z')
if nargin >3
if isfield(task, 'name')
    task_name = task.name;
    robot_name = task.robot_name;
    title([task_name,' (',robot_name,'); Time: 0.00s'])
end
end
axis equal
view([1,1,1]);
grid
f.Position = [1 80 1440 725];

%% Animate (only if q_sim is a timeseries)
p = fwdkin(q,DHtab,jt);
trace = repmat(p',500,1);
trace_plot=plot3(trace(:,1),trace(:,2),trace(:,3),'color','red','linewidth',2); % plot the trace

if isa(q_sim,'timeseries')
    % Process trajectory
    tSample = 0.05;
    timeVector = q_sim.Time(1):tSample:q_sim.Time(end);
    q_sim = resample(q_sim,timeVector);
    
    for i=1:size(q_sim.Data,1)
        if nargin>3
        title([task_name,' (',robot_name,'); Time: ',num2str(timeVector(i),'%.2f'),'s'])
        end

        % Compute enclosures at q
        q = q_sim.Data(i,:)';
        
        % Plot trace
        p = fwdkin(q,DHtab,jt);
        trace = circshift(trace,-1,1);
        trace(end,:) = p';
        trace_plot.XData = trace(:,1);
        trace_plot.YData = trace(:,2);
        trace_plot.ZData = trace(:,3);
        
        switch modus
            case 1
                robotOccupanciesV = fwdkinCollisionModel(q,DHext,EnclosuresV,'vertices');
            case 2
                robotOccupanciesC = fwdkinCollisionModel(q,DHext,EnclosuresC,'capsules');
            case 3
                robotOccupanciesB = fwdkinCollisionModel(q,DHext,EnclosuresB,'box');
            case 4
                robotOccupanciesV = fwdkinCollisionModel(q,DHext,EnclosuresV,'vertices');
                robotOccupanciesC = fwdkinCollisionModel(q,DHext,EnclosuresC,'capsules');
        end
%         
%         robotOccupanciesC = fwdkinCollisionModel(q,DHext,EnclosuresC,'capsules');
%         robotOccupanciesV = fwdkinCollisionModel(q,DHext,EnclosuresV,'vertices');
%         robotOccupanciesB = fwdkinCollisionModel(q,DHext,EnclosuresB,'box');

        for j=1:N+1

            % plot convex hull of vertices
            if modus == 1 || modus == 4
                if ~isempty(robotOccupanciesV{j})
                    k = convhulln(robotOccupanciesV{j}');
                    s(j).Faces = k;
                    s(j).Vertices = robotOccupanciesV{j}';
                    % Collision detection
                    if nargin>3
                        dist2obstacle = vecnorm(robotOccupanciesV{j} - obstacle(1:3)',2,1);
                        if any(dist2obstacle < obstacle(4))
                            s(j).FaceColor = [1 0.62 0]; %Orange when collision
                        else
                            s(j).FaceColor = [0 1 1]; %blue when no collision
                        end
                    end
                end
            end
            
            % plot box
            if modus == 3
                if ~isempty(robotOccupanciesB{j})
                    k = convhulln(robotOccupanciesB{j}');
                    s(j).Faces = k;
                    s(j).Vertices = robotOccupanciesB{j}';
                end
            end

            % plot capsules
            if modus == 2 || modus == 4
                [x1,y1,z1,x2,y2,z2,xc,yc,zc] = meshCapsule(robotOccupanciesC(:,j));
                s1(j).XData = x1;
                s1(j).YData = y1;
                s1(j).ZData = z1;
                s2(j).XData = x2;
                s2(j).YData = y2;
                s2(j).ZData = z2;
                sc(j).XData = xc;
                sc(j).YData = yc;
                sc(j).ZData = zc;
            end
        end
        pause(tSample)
    end
end
