% Jacobian - Calculation of the geometric Jacobian given DH-Table
%
% Syntax:  
%    J = Jacobian(q,DHtab,type)
%
% Inputs: 
%    q     -> Current Joint position vector (n x 1 matrix)
%    DHtab -> Standard DH table, the i-th row of DHtab is
%             [a_i, alpha_i, gamma_i, d_i].
%    jt    -> (optional) Id vector of joint types (0 for rev., 1 for prism.)
%
% Outputs:
%    J     -> Geometric Jacobian (6 x n Matrix)
%
% Example: 
%
% Other m-files required: none
% Subfunctions: dh_trans
% MAT-files required: none
%
% See also: 
% 

% Author:       Stefan Liu
% Written:      31-December-2015
% Last update:  ---
% Last revision:---

%------------- BEGIN CODE --------------
function [J] = Jacobian(q,DHtab,jt)
%#codegen

%% Initialization
[N, ~]= size(DHtab);
prismatic = 1;
revolute = 0;

if nargin < 3 %set default value for type
    jt = revolute*ones(N,1);
end

%Define Input
a = DHtab(:,1);
alpha = DHtab(:,2);
theta = DHtab(:,3) + not(jt).*q ;
d = DHtab(:,4) + jt.*q;

%Define Output and auxiliary variables
if isnumeric(DHtab)%Numeric Calculation
    J = zeros(6,N);
    z = zeros(3,N+1);
    p = zeros(3,N+1);
else %Symbolic Calculation
    J = sym(zeros(6,N));
    z = sym(zeros(3,N+1));
    p = sym(zeros(3,N+1));
end
T_a = eye(4);
z(:,1) = [0;0;1];
p(:,1) = [0;0;0];
p_0 = [0;0;0;1];

%% Calculate Position Vectors p_e, p and z
for i=1:N
    T = dh_trans(a(i),alpha(i),theta(i),d(i));
    T_a = T_a*T;
    p_tilde = T_a*p_0;
    p(:,i+1) = p_tilde(1:3);
    z(:,i+1) = T_a(1:3,1:3)*z(:,1);
end
p_e = p(:,N+1);

%% Fill-in the Jacobian J
for i=1:N
    if jt(i) == prismatic
        J(1:3,i) = z(:,i);
        J(4:6,i) = [0;0;0];
    else%jt(i) == revolute
        J(1:3,i) = cross(z(:,i),p_e - p(:,i));
        J(4:6,i) = z(:,i);
    end
end
end

%% DH-Helper
% D - homogeneous transformation matrix of a system in i into a system in i-1
function D = dh_trans(a, alpha,theta, d)

D=[cos(theta), -sin(theta)*cos(alpha), sin(theta)*sin(alpha), a*cos(theta);...
    sin(theta), cos(theta)*cos(alpha), -cos(theta)*sin(alpha), a*sin(theta);...
    0, sin(alpha), cos(alpha), d;...
    0,0,0,1];

end
