% ModRob2DHext -  Returns the extended D-H table using information in ModRob
%
% Syntax:
%    ModRob2DHext(ModRob)
%
% Inputs:
%    ModRob -> Ordered array of the module structures with respect to
%              the robot assembly
%
% Outputs:
%    DHext_out -> Extended DH table of parameters, the i-th row is
%                 [a_i, alpha_i, gamma_i, d_i, p_i, n_i, jt_i, phi_i],
%                 (refer to [1] for further details on the parameters).
%    B         -> B is the hom. transf. matrix from the basis to the
%                 first joint
%
% Example:
%
% Other m-files required: Tr(axis,L), Ro(axis,angle)
% Subfunctions: compute_ModKinInfov2(F,phi_im1,delJ_im1,guard)
% MAT-files required: none
%
% See also:
% [1] A. Giusti and M. Althoff, "Automatic Centralized Controller Design for
% Modular and Reconfigurable Robot Manipulators", IROS 2015.

% Author:       Andrea Giusti
% Written:      14-December-2015
% Last update:  Andrea Giusti 05.03.2017
%               Stefan Liu 18.06.2018
% Last revision:---

%------------- BEGIN CODE --------------

function [DHext_out,B] = ModRob2DHext(ModRob)
%% Number of robot links
NJ = get_NJ(ModRob); %Number of joints
NA = NJ + 1; % Number of robot links
ND = get_ND(ModRob); % Number of Modules (a joint module with n axes is decomposed into n joint modules)

%% Group Modules into robot links (step 2)
phi      = zeros(length(NJ),1);
kin_info = zeros(length(NJ),5);
DHext    = zeros(length(NJ),8);

% modules_of_link = group_modules(ModRob,NA,ND);
modules_of_link_indices = group_modules(ModRob,NA,ND);


%% Assemble
for i=1:NA
    j = 1;
    T = eye(4);
    while j<=length(modules_of_link_indices{i}) % iterate through all modules
        % get transformation matrix
        T_j = get_transformMatrixAndGeometry(ModRob(modules_of_link_indices{i}(j)),i,j);
        T = T*T_j;        
        j=j+1;
    end
    
    % finally, align x-Axis to DHext-Convention
    if i>1 % Do not consider Base!
        cntJ = i-1;
        [phi(cntJ),guard] = get_phi(T);
        T = T*Ro('z', phi(cntJ));

        % fill in DHext
        if cntJ > 1
            kin_info(cntJ,:) = compute_ModKinInfov2(T,phi(cntJ-1),ModRob(modules_of_link_indices{i}(1)).Kj.delta_j,guard); %kin_info = [a_0 alpha_0 gamma_0 p_0 n_0];
            DHext(cntJ,1:7) = [kin_info(cntJ,1) kin_info(cntJ,2) kin_info(cntJ,3) kin_info(cntJ-1,5)-kin_info(cntJ,4) kin_info(cntJ,4) kin_info(cntJ,5) ModRob(modules_of_link_indices{i}(1)).Kj.jt];
            DHext(cntJ,8) = phi(cntJ-1);
        else
            kin_info(cntJ,:) = compute_ModKinInfov2(T,0,ModRob(modules_of_link_indices{i}(1)).Kj.delta_j,guard);
            DHext(cntJ,1:7) = [kin_info(cntJ,1) kin_info(cntJ,2) kin_info(cntJ,3) -kin_info(cntJ,4) kin_info(cntJ,4) kin_info(cntJ,5) ModRob(modules_of_link_indices{i}(1)).Kj.jt];
        end
    else
        B = T;
    end
end

DHext_out = DHext;

end

% Function: Number of Modules (a joint module with n axes is decomposed into n joint modules)
function ND = get_ND(ModRob)
ND =  0;
for k =1:length(ModRob)
    if ModRob(k).Mod.ID ~= 0
        ND = ND+1;
    end
end
end

% Function: Group Modules into robot links
function modules_of_link_indices = group_modules(ModRob,NA,ND)

modules_of_link_indices = cell(1,NA);
j = 1;
% fill in for each robot link
index_next = [];
for i = 1:NA
    index = index_next;
    if j<=length(ModRob)
        while ModRob(j).Mod.typ ~= 1 && j < ND %if nor joint module or end effector
            index = [index,j];
            j = j+1;
        end
        index = [index,j];
    end
    modules_of_link_indices{i} = index;
    if j<=ND %joint module is also part of the next robot link except if endeffector
        index_next = j;
        j = j+1;
    end
end
end

% Function: Get Modular Transformation matrix
function [T] = get_transformMatrixAndGeometry(module,armNo,modNo)

% Case: Link module
if module.Mod.typ ~= 1
    p         = module.Kpl.p_pl;
    a         = module.Kpl.a_pl;
    n         = module.Kpl.n_pl;
    alpha     = module.Kpl.alpha_pl;
    delta_in  = module.Kpl.delta_pl;
    delta_out = module.Kdl.delta_dl;
else
    % Case: Joint module
        
    % Joint module is first of this robot link
    if modNo == 1 && armNo ~= 1
        p     = module.Kdl.p_dl;
        a     = module.Kdl.a_dl;
        n     = module.Kdl.n_dl;
        alpha     = module.Kdl.alpha_dl;
        delta_in = 0;
        delta_out = module.Kdl.delta_dl;
        
    % Joint module is last of this robot link OR
    % Joint module is first of the entire robot (proximal part is base)
    else
        p     = module.Kpl.p_pl;
        a     = module.Kpl.a_pl;
        n     = module.Kpl.n_pl;
        alpha = module.Kpl.alpha_pl;
        delta_in = module.Kpl.delta_pl;
        delta_out = 0;
    end
end

% Transformation Matrix
% T = Ro('z',-delta_in)*Tr('z',-p)*Tr('x',a)...
%     *Ro('x',alpha)*Tr('z',n)*Ro('z',delta_out);
sin_alpha = sin(alpha);
cos_alpha = cos(alpha);
sin_delta_in = sin(delta_in);
cos_delta_in = cos(delta_in);
sin_delta_out = sin(delta_out);
cos_delta_out = cos(delta_out);
T = zeros(4,4);
T(1,:)=[ cos_delta_in*cos_delta_out + cos_alpha*sin_delta_in*sin_delta_out, cos_alpha*cos_delta_out*sin_delta_in - cos_delta_in*sin_delta_out, -sin_alpha*sin_delta_in,   a*cos_delta_in - n*sin_alpha*sin_delta_in];
T(2,:)=[ cos_alpha*cos_delta_in*sin_delta_out - cos_delta_out*sin_delta_in, sin_delta_in*sin_delta_out + cos_alpha*cos_delta_in*cos_delta_out, -cos_delta_in*sin_alpha, - a*sin_delta_in - cos_delta_in*n*sin_alpha];
T(3,:)=[                                           sin_alpha*sin_delta_out,                                           cos_delta_out*sin_alpha,               cos_alpha,                             cos_alpha*n - p];
T(4,:)=[                                                                 0,                                                                 0,                       0,                                           1];

end

% Function: Get phi such that Transformation T*Ro('phi') aligns with DHext-Transformation
function [phi,guard] = get_phi(F_aux)
zim1 = [0 0 1]';
zi = F_aux(1:3,3);
pxyz_im1 = F_aux(1:3,4);
Rim1_i = F_aux(1:3,1:3);
% to avoid numerical problems we introduce a guard
guard = 1e-3;
% init impact that a delta has on the next module
%     phi = 0;
% Case 1: zim1 and zi are overlapped
if (norm(cross(zi,zim1)) < guard) && (norm(pxyz_im1(1:2)) < guard)
    phi = 0;
    % Case 2: zim1 and zi are parallel
elseif norm(cross(zi,zim1)) < guard
    % Be careful, the angle is expressed in frame im1 but the
    % rotation must be performed in frame i, so that the
    % angle in i must be determined.
    pxyz_i = transpose(Rim1_i)*pxyz_im1;
    phi = atan2(pxyz_i(2),pxyz_i(1));
    % Case 3: zim1 and zi are skew or intersect
else
    nx_im1 = abs(cross(zi,zim1));
    nx_i = transpose(Rim1_i)*nx_im1;
    phi = atan2(nx_i(2),nx_i(1));
end
end