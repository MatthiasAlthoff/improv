function [alpha,a,n,p,delta_in,delta_out] = Trafo2ModDH(T)
% Determine modular DH parameters from a transformation matrix
% Input:        T - 4x4 homogeneous transformation matrix
% Output:       ModDH - Modular parameters:
%               [alpha,a,n,p,delta_in,delta_out]
%
% Author:   Stefan Liu
% Date:     25.10.2018


if abs(abs(T(3,3))-1)>0.0001
    alpha = acos(T(3,3));
    sin_alpha = sin(alpha);
    delta_in = atan2(T(1,3)/-sin_alpha,T(2,3)/-sin_alpha);
    delta_out = atan2(T(3,1)/sin_alpha,T(3,2)/sin_alpha);
    
    A = [cos(delta_in), T(1,3);  -sin(delta_in), T(2,3)];
    sol = pinv(A)*[T(1,4);T(2,4)];
    a = sol(1);
    n = sol(2);
    p = n*T(3,3) - T(3,4);
else % if alpha == 0
    if sign(T(3,3)) == 1
        alpha = 0;
    else
        alpha = pi;
    end
    n = 0;
    p = -T(3,4);
    
    delta_in = atan2(-T(2,4),T(1,4));
    delta_out = -1*sign(T(3,3))*(atan2(-T(2,1),T(1,1))-delta_in);
    a = (T(1,4)+T(2,4))/(cos(delta_in)-sin(delta_in));
end

% s_io = (T(2,1)+T(1,2))/(T(3,3)-1);
% c_io = (T(1,1)+T(2,2))/(1-T(3,3));
% delta_io = atan2(s_io,c_io);

end