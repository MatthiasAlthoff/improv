
read_data;

% remove the incorrectly implemented subjects
erlaubt = [3:30];
X=DATA(erlaubt,2);
for i =1:28
    if(X(i)==2)
        GROUPS{i}='SV';
    else
        GROUPS{i}='SZ';
    end
end
erlaubt_2 = 1:15;
erlaubt_7 = 3:15;
robot_SV = robotTimes2(erlaubt_2,:);
robot_SZ = robotTimes7(erlaubt_7,:);
human_SV = humanTimes2(erlaubt_2,:);
human_SZ = humanTimes7(erlaubt_7,:);
human_both = humanTimes(erlaubt,:);
robot_both = robotTimes(erlaubt,:);
fprintf('for SV, N=%i\n',length(erlaubt_2))
fprintf('for SZ, N=%i\n\n',length(erlaubt_7))

%% Is the idle time of the robot different with SV and SZ?

%  Shapiro-Wilk test for normality.
SW1 = swtest(mean(robot_SZ(:,9:12),2));
SW2 = swtest(mean(robot_SZ(:,1:4),2));
SW3 = swtest(mean(robot_SZ,2));
SW4 = swtest(mean(robot_SV(:,9:12),2));
SW5 = swtest(mean(robot_SV(:,1:4),2));
SW6 = swtest(mean(robot_SV,2));

% if not normally distributed, do nonparametric test, else do ANOVA
if any([SW1,SW2,SW3,SW4,SW5,SW6])
    disp(['distribution of robot idle time is ',...
    'significantly different from normal']);
    pFunc = @kruskalwallis;
else
    disp(['distribution of robot idle time is ',...
    'not significantly different from normal']);
    pFunc = @anova1;
end

p_14 = pFunc(mean(robot_both(:,1:4),2),GROUPS,'off');
p_912 = pFunc(mean(robot_both(:,9:12),2),GROUPS,'off');
p_all = pFunc(mean(robot_both,2),GROUPS,'off');
improvement_trials_14 = (median(mean(robot_SZ(:,1:4),2)) - median(mean(robot_SV(:,1:4),2)))/median(mean(robot_SZ(:,1:4),2));
improvement_trials_912 = (median(mean(robot_SZ(:,9:12),2)) - median(mean(robot_SV(:,9:12),2)))/median(mean(robot_SZ(:,9:12),2));
improvement_trials_all = (median(mean(robot_SZ,2)) - median(mean(robot_SV,2)))/median(mean(robot_SZ,2));

if p_14 <= 0.05
    fprintf('Robot idle time is %4.1f percent lower in SV than SZ with p = %f, for the first 4 trials\n',improvement_trials_14*100,p_14)
else
    fprintf('Robot idle time is not significantly different in SV and SZ, for the first 4 trials\n')
end
if p_912 <= 0.05
    fprintf('Robot idle time is %4.1f percent lower in SV than SZ with p = %f, for the last 4 trials\n',improvement_trials_912*100,p_912)
else
    fprintf('Robot idle time is not significantly different in SV and SZ, for the last 4 trials\n')
end
if p_all <= 0.05
    fprintf('Robot idle time is %4.1f percent lower in SV than SZ with p = %f, over all trials\n',improvement_trials_all*100,p_all)
else
    fprintf('Robot idle time is not significantly different in SV and SZ, over all trials\n')
end

%% plot some graphs

% first 4 trials
pFunc(mean(robot_both(:,1:4),2),GROUPS);
set(gca,'fontsize',20)
axis([0.5 2.5 25 80])
ylabel('Idle time (seconds)')

% last 4 trials
pFunc(mean(robot_both(:,9:12),2),GROUPS);
set(gca,'fontsize',20)
axis([0.5 2.5 25 80])
ylabel('Idle time (seconds)')

% all trials
pFunc(mean(robot_both(:,9:12),2),GROUPS);
set(gca,'fontsize',20)
axis([0.5 2.5 25 80])
ylabel('Idle time (seconds)')



%% is the human TTC different between SV and SZ?

%  Shapiro-Wilk test for normality.
SW1 = swtest(mean(human_SZ(:,9:12),2));
SW2 = swtest(mean(human_SZ(:,1:4),2));
SW3 = swtest(mean(human_SZ,2));
SW4 = swtest(mean(human_SV(:,9:12),2));
SW5 = swtest(mean(human_SV(:,1:4),2));
SW6 = swtest(mean(human_SV,2));

% if not normally distributed, do nonparametric test, else do RANOVA
if any([SW1,SW2,SW3,SW4,SW5,SW6])
    disp(['distribution of human time to completion is ',...
    'significantly different from normal']);
    pFunc = @kruskalwallis;
    avFunc = @median;
else
    disp(['distribution of human time to completion is ',...
    'not significantly different from normal']);
    pFunc = @anova1;
    avFunc = @mean;
end
 
improvement_trials_14 = (avFunc(mean(human_SZ(:,1:4),2)) - avFunc(mean(human_SV(:,1:4),2)))/avFunc(mean(human_SZ(:,1:4),2));
improvement_trials_912 = (avFunc(mean(human_SZ(:,9:12),2)) - avFunc(mean(human_SV(:,9:12),2)))/avFunc(mean(human_SZ(:,9:12),2));
improvement_trials_all = (avFunc(mean(human_SZ,2)) - avFunc(mean(human_SV,2)))/avFunc(mean(human_SZ,2));
p_14 = pFunc(mean(human_both(:,1:4),2),GROUPS,'off');
p_912 = pFunc(mean(human_both(:,9:12),2),GROUPS,'off');
p_all = pFunc(mean(human_both,2),GROUPS,'off');

if p_14 <= 0.05
    fprintf('Human TTC is %4.1f percent lower in SV than SZ with p = %f, for the first 4 trials\n',improvement_trials_14,p_14)
else
    fprintf('Human TTC is not significantly different in SV and SZ, for the first 4 trials\n')
end
if p_912 <= 0.05
    fprintf('Human TTC is  %4.1f percent lower in SV than SZ with p = %f, for the last 4 trials\n',improvement_trials_912,p_912)
else
    fprintf('Human TTC is not significantly different in SV and SZ, for the last 4 trials\n')
end
if p_all <= 0.05
    fprintf('Human TTC is  %4.1f percent lower in SV than SZ with p = %f, over all trials\n',improvement_trials_all,p_all)
else
    fprintf('Human TTC is not significantly different in SV and SZ, over all trials\n')
end

%% Does the idle time of the robot decrease with trials?
% Shapiro-Wilk test on distribution of mean robot idle times
% averaged over all trials
SW1= swtest(mean(robot_SV,2));
SW2= swtest(mean(robot_SZ,2));

% if not normally distributed, do nonparametric test, else do RANOVA
if any([SW1,SW2])
    disp(['distribution of mean robot idle times averaged over',...
    ' all trials significantly different from normal']);
    pFunc = @friedman;
    avFunc = @median;
else
    disp(['distribution of mean robot idle times averaged over',...
    ' all trials not significantly different from normal']);
    pFunc = @ranova;
    avFunc = @mean;
end

[p_r2] = pFunc(robot_SV);
[p_r7] = pFunc(robot_SZ);

Y1 = avFunc(robot_SV);
improvement_r2 = (Y1(1)-Y1(12))/Y1(1);
Y2 = avFunc(robot_SZ);
improvement_r7 = (Y2(1)-Y2(12))/Y2(1);

fprintf('average decrease in time over 12 trials of robot in SV is %2.1f percent with p = %f\n',improvement_r2*100,p_r2)
fprintf('average decrease in time over 12 trials of robot in SZ is %2.1f percent with p = %f\n',improvement_r7*100,p_r7)

%% plot improvement

figure; hold on;
plot(Y1)
plot(Y2)
legend('SV', 'SZ')
set(gca,'fontsize',20)
ylabel('Idle time (seconds)')
xlabel('Trial number')

%% Does the idle time of the robot and the TTC of the human decrease with trials?
% Shapiro-Wilk test on distribution of mean robot and human idle times
% averaged over all trials
SW1= swtest(mean(human_SV,2));
SW2= swtest(mean(human_SZ,2));

% if not normally distributed, do nonparametric test, else do RANOVA
if any([SW1,SW2])
    disp(['distribution of mean human times averaged over',...
    ' all trials significantly different from normal']);
    pFunc = @friedman;
    avFunc = @median;
else
    disp(['distribution of mean human times averaged over',...
    ' all trials not significantly different from normal']);
    pFunc = @ranova;
    avFunc = @mean;
end

[p_h2] = pFunc(human_SV);
[p_h7] = pFunc(human_SZ);

Y1 = avFunc(human_SV);
improvement_h2 = (Y1(1)-Y1(12))/Y1(1);
Y2 = avFunc(human_SZ);
improvement_h7 = (Y2(1)-Y2(12))/Y2(1);

fprintf('average decrease in time over 12 trials of human in SV is %2.1f percent with p = %f\n',improvement_h2*100,p_h2)
fprintf('average decrease in time over 12 trials of human in SZ is %2.1f percent with p = %f\n',improvement_h7*100,p_h7)

%% plot improvement

figure; hold on;
plot(Y1)
plot(Y2)
legend('SV', 'SZ')
set(gca,'fontsize',20)
ylabel('Time to completion (seconds)')
xlabel('Trial number')

