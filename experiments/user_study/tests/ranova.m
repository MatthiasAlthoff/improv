function [p, F] = ranova(T,varargin)
% repeated measures analysis of means
% T is a matrix with each row a different subject and each column a
% different measurement time, e.g. before and after
% this performs ANOVA with correlated examples

% preprocess T to omit NRPs
allowedColumns = sum((T > 0),2) == size(T,2);
T = T(allowedColumns,:);

k = size(T,2);
n = size(T,1);

SubjectMeans = sum(T,2)/k;
GroupMeans = sum(T,1)/n;
GrandMean = sum(GroupMeans)/k;

SSw = sum(sum((T - repmat(GroupMeans,n,1)).^2));
SSs = k * sum((SubjectMeans - GrandMean).^2);

SSe = SSw-SSs;

SStime = n * sum((GroupMeans - GrandMean).^2);

MStime = SStime / (k - 1);

MSe = SSe / ((k - 1) * (n - 1));

F = MStime/MSe;


p = 1- fcdf(F,(k - 1),(k - 1) * (n - 1));