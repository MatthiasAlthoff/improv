README

Below are a description of the methods used to evaluate the results found in the Science Robotics Paper (Althoff et Al., submitted 2018) and the PhD dissertation of Aaron Pereira, how to execute the code, and exclusions and corrections to the data. 

N = 13 for SRMS
N = 15 for SSM

Time measured in seconds.

Normality was estimating using the Shapiro-Wilk test with alpha = 0.05. Where normality was violated, non-parametric tests were used (Kruskal-Wallis and Friedman's) and where not, an ANOVA or repeated means ANOVA was used. Two-sided testing was used.

-Aaron Pereira, Nov. 2018

Execution:

This code can be executed on MATLAB 2016b and requires the statistic toolbox. All code is my own (AP) except the functions "cronbach.m" and "swtest.m", which were written by Alexandros Leontitsis (leoaleq@yahoo.com) and Ahmed ben Saida (ahmedbensaida@yahoo.com) respectively.

Navigate to this folder in MATLAB. The file you want to execute is "test_hypotheses.m". Everything else is self-explanatory or explained in the functions themselves.

Exclusions and corrections:

The control for subjects 1 and 2 was not implemented correctly. Therefore, these subjects were excluded from the analysis.

The time logging code for the "robot idle time" had two bugs. The first meant that the recorded time was off by a multiple of 3.4. This was fixed after subject 9. The corrections were found by comparing the logged data with the video recordings, and were saved in corrections.csv

The second meant that robot idle time during the last movement of the robot was not logged. The affected logs were identified and the missing time found from the video recordings. The corrections are saved in corrections2.csv