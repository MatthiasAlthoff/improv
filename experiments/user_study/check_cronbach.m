function alpha = check_cronbach(Data,items)
%% Script checks the cronbach alpha

alpha = cronbach(Data(:,items));
