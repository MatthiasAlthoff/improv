
%% read in data
DATA = csvread('times.csv');
robotTimes = DATA(:,5:2:end);
humanTimes = DATA(:,4:2:end);
% corrections due to logging bug 1
corr = [dlmread('corrections.csv',',',[0 1 29 8]),zeros(30,4)];
% corrections due to logging bug 2
corr2 = [dlmread('corrections2.csv',',',[0 1 29 12])];
% add corrections
robotTimes = robotTimes + corr + corr2;

%% percent improved
pcImproveRobot = 1-[mean(robotTimes(:,7:12),2)./mean(robotTimes(:,1:4),2)];
pcImproveHuman = 1-[mean(humanTimes(:,7:12),2)./mean(humanTimes(:,1:4),2)];


%% split into robot time and human time for different strategies 
% (each row is a different subject; each column is a different trial)
index2 = find(DATA(:,2) == 2);
index7 = find(DATA(:,2) ~= 2);
robotTimes2 = robotTimes(index2,:);
robotTimes7 = robotTimes(index7,:);
humanTimes2 = humanTimes(index2,:);
humanTimes7 = humanTimes(index7,:);
pcImproveRobot2 = pcImproveRobot(index2,:);
pcImproveRobot7 = pcImproveRobot(index7,:);
pcImproveHuman2 = pcImproveHuman(index2,:);
pcImproveHuman7 = pcImproveHuman(index7,:);
